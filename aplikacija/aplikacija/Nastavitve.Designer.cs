﻿namespace aplikacija
{
    partial class Nastavitve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mongoDB_groupbox = new System.Windows.Forms.GroupBox();
            this.mongoDBsaveConnection_button = new System.Windows.Forms.Button();
            this.collection_label = new System.Windows.Forms.Label();
            this.collection_textbox = new System.Windows.Forms.TextBox();
            this.database_label = new System.Windows.Forms.Label();
            this.database_textbox = new System.Windows.Forms.TextBox();
            this.connectionString_checkbox = new System.Windows.Forms.CheckBox();
            this.connectionString_label = new System.Windows.Forms.Label();
            this.connectionString_textbox = new System.Windows.Forms.TextBox();
            this.port_label = new System.Windows.Forms.Label();
            this.portMongoDB_textbox = new System.Windows.Forms.TextBox();
            this.ipAddressMongoDB_label = new System.Windows.Forms.Label();
            this.ipAddressMongoDB_textbox = new System.Windows.Forms.TextBox();
            this.password_label = new System.Windows.Forms.Label();
            this.password_textbox = new System.Windows.Forms.TextBox();
            this.mongoDBConnectionTest_button = new System.Windows.Forms.Button();
            this.username_label = new System.Windows.Forms.Label();
            this.username_textbox = new System.Windows.Forms.TextBox();
            this.predictionIO_groupbox = new System.Windows.Forms.GroupBox();
            this.predictionIOAccessKey_label = new System.Windows.Forms.Label();
            this.predictionIOAccessKey_textbox = new System.Windows.Forms.TextBox();
            this.predictionIOsaveConnection_button = new System.Windows.Forms.Button();
            this.portPredictionIOEngineServer_label = new System.Windows.Forms.Label();
            this.portPredictionIOEngineServer_textbox = new System.Windows.Forms.TextBox();
            this.portPredictionIOEventServer_label = new System.Windows.Forms.Label();
            this.portPredictionIOEventServer_textbox = new System.Windows.Forms.TextBox();
            this.ipAddressPredictionIO_label = new System.Windows.Forms.Label();
            this.ipAddressPredictionIO_textbox = new System.Windows.Forms.TextBox();
            this.predictionIOConnectionTest_button = new System.Windows.Forms.Button();
            this.console = new System.Windows.Forms.RichTextBox();
            this.PredictionIO_StatusAPI = new System.Windows.Forms.Label();
            this.PredictionIOEngine = new System.Windows.Forms.Label();
            this.PredictionIO_Status = new System.Windows.Forms.Label();
            this.PredictionIO = new System.Windows.Forms.Label();
            this.MongoDB_Status = new System.Windows.Forms.Label();
            this.MongoDB = new System.Windows.Forms.Label();
            this.mongoDB_groupbox.SuspendLayout();
            this.predictionIO_groupbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // mongoDB_groupbox
            // 
            this.mongoDB_groupbox.Controls.Add(this.mongoDBsaveConnection_button);
            this.mongoDB_groupbox.Controls.Add(this.collection_label);
            this.mongoDB_groupbox.Controls.Add(this.collection_textbox);
            this.mongoDB_groupbox.Controls.Add(this.database_label);
            this.mongoDB_groupbox.Controls.Add(this.database_textbox);
            this.mongoDB_groupbox.Controls.Add(this.connectionString_checkbox);
            this.mongoDB_groupbox.Controls.Add(this.connectionString_label);
            this.mongoDB_groupbox.Controls.Add(this.connectionString_textbox);
            this.mongoDB_groupbox.Controls.Add(this.port_label);
            this.mongoDB_groupbox.Controls.Add(this.portMongoDB_textbox);
            this.mongoDB_groupbox.Controls.Add(this.ipAddressMongoDB_label);
            this.mongoDB_groupbox.Controls.Add(this.ipAddressMongoDB_textbox);
            this.mongoDB_groupbox.Controls.Add(this.password_label);
            this.mongoDB_groupbox.Controls.Add(this.password_textbox);
            this.mongoDB_groupbox.Controls.Add(this.mongoDBConnectionTest_button);
            this.mongoDB_groupbox.Controls.Add(this.username_label);
            this.mongoDB_groupbox.Controls.Add(this.username_textbox);
            this.mongoDB_groupbox.Location = new System.Drawing.Point(12, 44);
            this.mongoDB_groupbox.Name = "mongoDB_groupbox";
            this.mongoDB_groupbox.Size = new System.Drawing.Size(310, 394);
            this.mongoDB_groupbox.TabIndex = 6;
            this.mongoDB_groupbox.TabStop = false;
            this.mongoDB_groupbox.Text = "MongoDB";
            // 
            // mongoDBsaveConnection_button
            // 
            this.mongoDBsaveConnection_button.Location = new System.Drawing.Point(90, 365);
            this.mongoDBsaveConnection_button.Name = "mongoDBsaveConnection_button";
            this.mongoDBsaveConnection_button.Size = new System.Drawing.Size(75, 23);
            this.mongoDBsaveConnection_button.TabIndex = 17;
            this.mongoDBsaveConnection_button.Text = "Shrani";
            this.mongoDBsaveConnection_button.UseVisualStyleBackColor = true;
            this.mongoDBsaveConnection_button.Click += new System.EventHandler(this.MongoDBsaveConnection_button_Click);
            // 
            // collection_label
            // 
            this.collection_label.AutoSize = true;
            this.collection_label.Location = new System.Drawing.Point(6, 314);
            this.collection_label.Name = "collection_label";
            this.collection_label.Size = new System.Drawing.Size(37, 13);
            this.collection_label.TabIndex = 16;
            this.collection_label.Text = "Zbirka";
            // 
            // collection_textbox
            // 
            this.collection_textbox.Location = new System.Drawing.Point(9, 330);
            this.collection_textbox.Name = "collection_textbox";
            this.collection_textbox.Size = new System.Drawing.Size(297, 20);
            this.collection_textbox.TabIndex = 15;
            // 
            // database_label
            // 
            this.database_label.AutoSize = true;
            this.database_label.Location = new System.Drawing.Point(6, 266);
            this.database_label.Name = "database_label";
            this.database_label.Size = new System.Drawing.Size(79, 13);
            this.database_label.TabIndex = 14;
            this.database_label.Text = "Baza podatkov";
            // 
            // database_textbox
            // 
            this.database_textbox.Location = new System.Drawing.Point(7, 282);
            this.database_textbox.Name = "database_textbox";
            this.database_textbox.Size = new System.Drawing.Size(297, 20);
            this.database_textbox.TabIndex = 13;
            // 
            // connectionString_checkbox
            // 
            this.connectionString_checkbox.AutoSize = true;
            this.connectionString_checkbox.Location = new System.Drawing.Point(150, 24);
            this.connectionString_checkbox.Name = "connectionString_checkbox";
            this.connectionString_checkbox.Size = new System.Drawing.Size(153, 17);
            this.connectionString_checkbox.TabIndex = 12;
            this.connectionString_checkbox.Text = "Uporaba niza za povezavo";
            this.connectionString_checkbox.UseVisualStyleBackColor = true;
            this.connectionString_checkbox.CheckedChanged += new System.EventHandler(this.ConnectionString_CheckedChanged);
            // 
            // connectionString_label
            // 
            this.connectionString_label.AutoSize = true;
            this.connectionString_label.Location = new System.Drawing.Point(6, 47);
            this.connectionString_label.Name = "connectionString_label";
            this.connectionString_label.Size = new System.Drawing.Size(86, 13);
            this.connectionString_label.TabIndex = 11;
            this.connectionString_label.Text = "Niz za povezavo";
            // 
            // connectionString_textbox
            // 
            this.connectionString_textbox.Location = new System.Drawing.Point(7, 63);
            this.connectionString_textbox.Name = "connectionString_textbox";
            this.connectionString_textbox.Size = new System.Drawing.Size(297, 20);
            this.connectionString_textbox.TabIndex = 10;
            // 
            // port_label
            // 
            this.port_label.AutoSize = true;
            this.port_label.Location = new System.Drawing.Point(6, 226);
            this.port_label.Name = "port_label";
            this.port_label.Size = new System.Drawing.Size(32, 13);
            this.port_label.TabIndex = 9;
            this.port_label.Text = "Vrata";
            // 
            // portMongoDB_textbox
            // 
            this.portMongoDB_textbox.Location = new System.Drawing.Point(6, 242);
            this.portMongoDB_textbox.Name = "portMongoDB_textbox";
            this.portMongoDB_textbox.Size = new System.Drawing.Size(297, 20);
            this.portMongoDB_textbox.TabIndex = 8;
            // 
            // ipAddressMongoDB_label
            // 
            this.ipAddressMongoDB_label.AutoSize = true;
            this.ipAddressMongoDB_label.Location = new System.Drawing.Point(6, 182);
            this.ipAddressMongoDB_label.Name = "ipAddressMongoDB_label";
            this.ipAddressMongoDB_label.Size = new System.Drawing.Size(53, 13);
            this.ipAddressMongoDB_label.TabIndex = 7;
            this.ipAddressMongoDB_label.Text = "IP Naslov";
            // 
            // ipAddressMongoDB_textbox
            // 
            this.ipAddressMongoDB_textbox.Location = new System.Drawing.Point(6, 198);
            this.ipAddressMongoDB_textbox.Name = "ipAddressMongoDB_textbox";
            this.ipAddressMongoDB_textbox.Size = new System.Drawing.Size(297, 20);
            this.ipAddressMongoDB_textbox.TabIndex = 6;
            // 
            // password_label
            // 
            this.password_label.AutoSize = true;
            this.password_label.Location = new System.Drawing.Point(6, 134);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(34, 13);
            this.password_label.TabIndex = 5;
            this.password_label.Text = "Geslo";
            // 
            // password_textbox
            // 
            this.password_textbox.Location = new System.Drawing.Point(6, 150);
            this.password_textbox.Name = "password_textbox";
            this.password_textbox.PasswordChar = '*';
            this.password_textbox.Size = new System.Drawing.Size(297, 20);
            this.password_textbox.TabIndex = 4;
            // 
            // mongoDBConnectionTest_button
            // 
            this.mongoDBConnectionTest_button.Location = new System.Drawing.Point(9, 365);
            this.mongoDBConnectionTest_button.Name = "mongoDBConnectionTest_button";
            this.mongoDBConnectionTest_button.Size = new System.Drawing.Size(75, 23);
            this.mongoDBConnectionTest_button.TabIndex = 3;
            this.mongoDBConnectionTest_button.Text = "Preizkus";
            this.mongoDBConnectionTest_button.UseVisualStyleBackColor = true;
            this.mongoDBConnectionTest_button.Click += new System.EventHandler(this.MongoDBConnectionTest_button_Click);
            // 
            // username_label
            // 
            this.username_label.AutoSize = true;
            this.username_label.Location = new System.Drawing.Point(6, 91);
            this.username_label.Name = "username_label";
            this.username_label.Size = new System.Drawing.Size(86, 13);
            this.username_label.TabIndex = 1;
            this.username_label.Text = "Uporabniško ime";
            // 
            // username_textbox
            // 
            this.username_textbox.Location = new System.Drawing.Point(6, 107);
            this.username_textbox.Name = "username_textbox";
            this.username_textbox.Size = new System.Drawing.Size(297, 20);
            this.username_textbox.TabIndex = 0;
            // 
            // predictionIO_groupbox
            // 
            this.predictionIO_groupbox.Controls.Add(this.predictionIOAccessKey_label);
            this.predictionIO_groupbox.Controls.Add(this.predictionIOAccessKey_textbox);
            this.predictionIO_groupbox.Controls.Add(this.predictionIOsaveConnection_button);
            this.predictionIO_groupbox.Controls.Add(this.portPredictionIOEngineServer_label);
            this.predictionIO_groupbox.Controls.Add(this.portPredictionIOEngineServer_textbox);
            this.predictionIO_groupbox.Controls.Add(this.portPredictionIOEventServer_label);
            this.predictionIO_groupbox.Controls.Add(this.portPredictionIOEventServer_textbox);
            this.predictionIO_groupbox.Controls.Add(this.ipAddressPredictionIO_label);
            this.predictionIO_groupbox.Controls.Add(this.ipAddressPredictionIO_textbox);
            this.predictionIO_groupbox.Controls.Add(this.predictionIOConnectionTest_button);
            this.predictionIO_groupbox.Location = new System.Drawing.Point(343, 44);
            this.predictionIO_groupbox.Name = "predictionIO_groupbox";
            this.predictionIO_groupbox.Size = new System.Drawing.Size(307, 262);
            this.predictionIO_groupbox.TabIndex = 17;
            this.predictionIO_groupbox.TabStop = false;
            this.predictionIO_groupbox.Text = "PredictionIO";
            // 
            // predictionIOAccessKey_label
            // 
            this.predictionIOAccessKey_label.AutoSize = true;
            this.predictionIOAccessKey_label.Location = new System.Drawing.Point(6, 157);
            this.predictionIOAccessKey_label.Name = "predictionIOAccessKey_label";
            this.predictionIOAccessKey_label.Size = new System.Drawing.Size(74, 13);
            this.predictionIOAccessKey_label.TabIndex = 20;
            this.predictionIOAccessKey_label.Text = "Dostopni ključ";
            // 
            // predictionIOAccessKey_textbox
            // 
            this.predictionIOAccessKey_textbox.Location = new System.Drawing.Point(6, 173);
            this.predictionIOAccessKey_textbox.Name = "predictionIOAccessKey_textbox";
            this.predictionIOAccessKey_textbox.Size = new System.Drawing.Size(297, 20);
            this.predictionIOAccessKey_textbox.TabIndex = 19;
            // 
            // predictionIOsaveConnection_button
            // 
            this.predictionIOsaveConnection_button.Location = new System.Drawing.Point(87, 216);
            this.predictionIOsaveConnection_button.Name = "predictionIOsaveConnection_button";
            this.predictionIOsaveConnection_button.Size = new System.Drawing.Size(75, 23);
            this.predictionIOsaveConnection_button.TabIndex = 18;
            this.predictionIOsaveConnection_button.Text = "Shrani";
            this.predictionIOsaveConnection_button.UseVisualStyleBackColor = true;
            this.predictionIOsaveConnection_button.Click += new System.EventHandler(this.predictionIOsaveConnection_button_Click);
            // 
            // portPredictionIOEngineServer_label
            // 
            this.portPredictionIOEngineServer_label.AutoSize = true;
            this.portPredictionIOEngineServer_label.Location = new System.Drawing.Point(7, 111);
            this.portPredictionIOEngineServer_label.Name = "portPredictionIOEngineServer_label";
            this.portPredictionIOEngineServer_label.Size = new System.Drawing.Size(66, 13);
            this.portPredictionIOEngineServer_label.TabIndex = 11;
            this.portPredictionIOEngineServer_label.Text = "Vrata za API";
            // 
            // portPredictionIOEngineServer_textbox
            // 
            this.portPredictionIOEngineServer_textbox.Location = new System.Drawing.Point(7, 127);
            this.portPredictionIOEngineServer_textbox.Name = "portPredictionIOEngineServer_textbox";
            this.portPredictionIOEngineServer_textbox.Size = new System.Drawing.Size(297, 20);
            this.portPredictionIOEngineServer_textbox.TabIndex = 10;
            // 
            // portPredictionIOEventServer_label
            // 
            this.portPredictionIOEventServer_label.AutoSize = true;
            this.portPredictionIOEventServer_label.Location = new System.Drawing.Point(6, 63);
            this.portPredictionIOEventServer_label.Name = "portPredictionIOEventServer_label";
            this.portPredictionIOEventServer_label.Size = new System.Drawing.Size(85, 13);
            this.portPredictionIOEventServer_label.TabIndex = 9;
            this.portPredictionIOEventServer_label.Text = "Vrata za strežnik";
            // 
            // portPredictionIOEventServer_textbox
            // 
            this.portPredictionIOEventServer_textbox.Location = new System.Drawing.Point(6, 79);
            this.portPredictionIOEventServer_textbox.Name = "portPredictionIOEventServer_textbox";
            this.portPredictionIOEventServer_textbox.Size = new System.Drawing.Size(297, 20);
            this.portPredictionIOEventServer_textbox.TabIndex = 8;
            // 
            // ipAddressPredictionIO_label
            // 
            this.ipAddressPredictionIO_label.AutoSize = true;
            this.ipAddressPredictionIO_label.Location = new System.Drawing.Point(6, 19);
            this.ipAddressPredictionIO_label.Name = "ipAddressPredictionIO_label";
            this.ipAddressPredictionIO_label.Size = new System.Drawing.Size(53, 13);
            this.ipAddressPredictionIO_label.TabIndex = 7;
            this.ipAddressPredictionIO_label.Text = "IP Naslov";
            // 
            // ipAddressPredictionIO_textbox
            // 
            this.ipAddressPredictionIO_textbox.Location = new System.Drawing.Point(6, 35);
            this.ipAddressPredictionIO_textbox.Name = "ipAddressPredictionIO_textbox";
            this.ipAddressPredictionIO_textbox.Size = new System.Drawing.Size(297, 20);
            this.ipAddressPredictionIO_textbox.TabIndex = 6;
            // 
            // predictionIOConnectionTest_button
            // 
            this.predictionIOConnectionTest_button.Location = new System.Drawing.Point(6, 216);
            this.predictionIOConnectionTest_button.Name = "predictionIOConnectionTest_button";
            this.predictionIOConnectionTest_button.Size = new System.Drawing.Size(75, 23);
            this.predictionIOConnectionTest_button.TabIndex = 3;
            this.predictionIOConnectionTest_button.Text = "Preizkus";
            this.predictionIOConnectionTest_button.UseVisualStyleBackColor = true;
            this.predictionIOConnectionTest_button.Click += new System.EventHandler(this.PredictionIOConnectionTest_button_Click);
            // 
            // console
            // 
            this.console.Location = new System.Drawing.Point(12, 463);
            this.console.Name = "console";
            this.console.Size = new System.Drawing.Size(657, 96);
            this.console.TabIndex = 18;
            this.console.Text = "";
            // 
            // PredictionIO_StatusAPI
            // 
            this.PredictionIO_StatusAPI.AutoSize = true;
            this.PredictionIO_StatusAPI.ForeColor = System.Drawing.Color.Red;
            this.PredictionIO_StatusAPI.Location = new System.Drawing.Point(569, 571);
            this.PredictionIO_StatusAPI.Name = "PredictionIO_StatusAPI";
            this.PredictionIO_StatusAPI.Size = new System.Drawing.Size(61, 13);
            this.PredictionIO_StatusAPI.TabIndex = 26;
            this.PredictionIO_StatusAPI.Text = "Ni povezan";
            // 
            // PredictionIOEngine
            // 
            this.PredictionIOEngine.AutoSize = true;
            this.PredictionIOEngine.Location = new System.Drawing.Point(431, 571);
            this.PredictionIOEngine.Name = "PredictionIOEngine";
            this.PredictionIOEngine.Size = new System.Drawing.Size(132, 13);
            this.PredictionIOEngine.TabIndex = 25;
            this.PredictionIOEngine.Text = "PredictionIO engine server";
            // 
            // PredictionIO_Status
            // 
            this.PredictionIO_Status.AutoSize = true;
            this.PredictionIO_Status.ForeColor = System.Drawing.Color.Red;
            this.PredictionIO_Status.Location = new System.Drawing.Point(340, 571);
            this.PredictionIO_Status.Name = "PredictionIO_Status";
            this.PredictionIO_Status.Size = new System.Drawing.Size(61, 13);
            this.PredictionIO_Status.TabIndex = 24;
            this.PredictionIO_Status.Text = "Ni povezan";
            // 
            // PredictionIO
            // 
            this.PredictionIO.AutoSize = true;
            this.PredictionIO.Location = new System.Drawing.Point(207, 571);
            this.PredictionIO.Name = "PredictionIO";
            this.PredictionIO.Size = new System.Drawing.Size(127, 13);
            this.PredictionIO.TabIndex = 23;
            this.PredictionIO.Text = "PredictionIO event server";
            // 
            // MongoDB_Status
            // 
            this.MongoDB_Status.AutoSize = true;
            this.MongoDB_Status.ForeColor = System.Drawing.Color.Red;
            this.MongoDB_Status.Location = new System.Drawing.Point(73, 571);
            this.MongoDB_Status.Name = "MongoDB_Status";
            this.MongoDB_Status.Size = new System.Drawing.Size(61, 13);
            this.MongoDB_Status.TabIndex = 22;
            this.MongoDB_Status.Text = "Ni povezan";
            // 
            // MongoDB
            // 
            this.MongoDB.AutoSize = true;
            this.MongoDB.Location = new System.Drawing.Point(9, 571);
            this.MongoDB.Name = "MongoDB";
            this.MongoDB.Size = new System.Drawing.Size(58, 13);
            this.MongoDB.TabIndex = 21;
            this.MongoDB.Text = "MongoDB:";
            // 
            // Nastavitve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 595);
            this.Controls.Add(this.PredictionIO_StatusAPI);
            this.Controls.Add(this.PredictionIOEngine);
            this.Controls.Add(this.PredictionIO_Status);
            this.Controls.Add(this.PredictionIO);
            this.Controls.Add(this.MongoDB_Status);
            this.Controls.Add(this.MongoDB);
            this.Controls.Add(this.console);
            this.Controls.Add(this.predictionIO_groupbox);
            this.Controls.Add(this.mongoDB_groupbox);
            this.Name = "Nastavitve";
            this.Text = "Nastavitve";
            this.Load += new System.EventHandler(this.Nastavitve_Load);
            this.mongoDB_groupbox.ResumeLayout(false);
            this.mongoDB_groupbox.PerformLayout();
            this.predictionIO_groupbox.ResumeLayout(false);
            this.predictionIO_groupbox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox mongoDB_groupbox;
        private System.Windows.Forms.CheckBox connectionString_checkbox;
        private System.Windows.Forms.Label connectionString_label;
        private System.Windows.Forms.TextBox connectionString_textbox;
        private System.Windows.Forms.Label port_label;
        private System.Windows.Forms.TextBox portMongoDB_textbox;
        private System.Windows.Forms.Label ipAddressMongoDB_label;
        private System.Windows.Forms.TextBox ipAddressMongoDB_textbox;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.TextBox password_textbox;
        private System.Windows.Forms.Button mongoDBConnectionTest_button;
        private System.Windows.Forms.Label username_label;
        private System.Windows.Forms.TextBox username_textbox;
        private System.Windows.Forms.Label collection_label;
        private System.Windows.Forms.TextBox collection_textbox;
        private System.Windows.Forms.Label database_label;
        private System.Windows.Forms.TextBox database_textbox;
        private System.Windows.Forms.GroupBox predictionIO_groupbox;
        private System.Windows.Forms.Label portPredictionIOEventServer_label;
        private System.Windows.Forms.TextBox portPredictionIOEventServer_textbox;
        private System.Windows.Forms.Label ipAddressPredictionIO_label;
        private System.Windows.Forms.TextBox ipAddressPredictionIO_textbox;
        private System.Windows.Forms.Button predictionIOConnectionTest_button;
        private System.Windows.Forms.Label portPredictionIOEngineServer_label;
        private System.Windows.Forms.TextBox portPredictionIOEngineServer_textbox;
        private System.Windows.Forms.RichTextBox console;
        private System.Windows.Forms.Button mongoDBsaveConnection_button;
        private System.Windows.Forms.Label PredictionIO_StatusAPI;
        private System.Windows.Forms.Label PredictionIOEngine;
        private System.Windows.Forms.Label PredictionIO_Status;
        private System.Windows.Forms.Label PredictionIO;
        private System.Windows.Forms.Label MongoDB_Status;
        private System.Windows.Forms.Label MongoDB;
        private System.Windows.Forms.Button predictionIOsaveConnection_button;
        private System.Windows.Forms.Label predictionIOAccessKey_label;
        private System.Windows.Forms.TextBox predictionIOAccessKey_textbox;
    }
}