﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aplikacija
{
    class Log
    {
        [JsonProperty]
        private string Timestamp;
        [JsonProperty]
        private string ClientID;
        [JsonProperty]
        private string MediaID;
        [JsonProperty]
        private List<string> Keywords;

        public Log()
        {
            Timestamp = "";
            ClientID = "";
            MediaID = "";
            Keywords = new List<string>();
        }
        public Log(string timestamp, string clientID, string mediaID, List<string> keywords)
        {
            Timestamp = timestamp;
            ClientID = clientID;
            MediaID = mediaID;
            Keywords = keywords;
        }
        public string getTimestamp() { return Timestamp; }
        public void setTimestamp(string _Timestamp) { Timestamp = _Timestamp; }
        public string getClientID() { return ClientID; }
        public void setClientID(string _ClientID) { ClientID = _ClientID; }
        public string getMediaID() { return MediaID; }
        public void seMediaID(string _MediaID) { MediaID = _MediaID; }
        public List<string> getKeywords() { return Keywords; }
        public void setKeywords(List<string> _Keywords) { Keywords = _Keywords; }
    }
}
