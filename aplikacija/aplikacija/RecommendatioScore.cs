﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aplikacija
{
    class RecommendatioScore
    {
        private string ID;
        private Dictionary<string, double> Scores;

        public RecommendatioScore()
        {
            ID = "";
            Scores = new Dictionary<string, double>();
        }
        public RecommendatioScore(string _ID)
        {
            ID = _ID;
            Scores = new Dictionary<string, double>();
        }
        public RecommendatioScore(string _ID, Dictionary<string,double> _Scores)
        {
            ID = _ID;
            Scores = _Scores;
        }

        public string getID() { return ID; }
        public void setID(string _ID) { ID = _ID; }

        public Dictionary<string, double> getScores() { return Scores; }
        public void setKeywords(Dictionary<string, double> _Scores) { Scores = _Scores; }

        public double getSpecificScore(string name)
        {
            if (Scores.ContainsKey(name))
                return Scores[name];
            else
                return 0;
        }
        
    }
}
