﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace aplikacija
{
    public partial class Nastavitve : Form
    {
        public bool dbClient_connection;
        public bool PredictionIO_serverConnection(string ip, string port)
        {
            using (var httpClient = new HttpClient())
            {
                WriteToConsole("Preizkus povezave v PredictionIO strežnik za podatke");
                Invoke(new Action(() => PredictionIO_Status.Text = "Poizkus.."));
                Invoke(new Action(() => PredictionIO_Status.ForeColor = Color.Orange));

                string path = "http://" + ip + ":" + port;

                try
                {
                    using (var request = new HttpRequestMessage(new HttpMethod("GET"), path))
                    {
                        var response = httpClient.SendAsync(request).Result;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = "Napaka: " + ex.ToString();
                    Invoke(new Action(() => PredictionIO_Status.Text = "Napaka"));
                    Invoke(new Action(() => PredictionIO_Status.ForeColor = Color.Red));
                    WriteToConsole(errorMessage);
                    return false;
                }
                Invoke(new Action(() => PredictionIO_Status.Text = "Dostopno"));
                Invoke(new Action(() => PredictionIO_Status.ForeColor = Color.Green));

                WriteToConsole("Povezava do PredictionIO event strežnika za podatke je dostopna");
                return true;

                
            }
        }

        public bool PredictionIO_engineConnection(string ip, string port)
        {
            using (var httpClient = new HttpClient())
            {
                WriteToConsole("Preizkus povezave v PredictionIO API strežnik");
                Invoke(new Action(() => PredictionIO_StatusAPI.Text = "Poizkus.."));
                Invoke(new Action(() => PredictionIO_StatusAPI.ForeColor = Color.Orange));

                string path = "http://" + ip + ":" + port;
                try
                {
                    using (var request = new HttpRequestMessage(new HttpMethod("GET"), path))
                    {
                        var response = httpClient.SendAsync(request).Result;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = "Napaka: " + ex.ToString();
                    Invoke(new Action(() => PredictionIO_StatusAPI.Text = "Napaka"));
                    Invoke(new Action(() => PredictionIO_StatusAPI.ForeColor = Color.Red));
                    WriteToConsole(errorMessage);
                    return false;
                }
                Invoke(new Action(() => PredictionIO_StatusAPI.Text = "Dostopno"));
                Invoke(new Action(() => PredictionIO_StatusAPI.ForeColor = Color.Green));

                WriteToConsole("Povezava do PredictionIO engine strežnika je dostopna");
                return true;
            }
        }
        public bool PreizkusPovezaveMongoDB(string connection_string, string bazaPodatkov, string zbirka)
        {
            WriteToConsole("Preizkus povezave v MongoDB podatkovno bazo");
            Invoke(new Action(() => MongoDB_Status.Text = "Preizkus.."));
            Invoke(new Action(() => MongoDB_Status.ForeColor = Color.Orange));
            try
            {
                var dbClient = new MongoClient(connection_string);
                var database = dbClient.GetDatabase(bazaPodatkov);
                bool isMongoLive = database.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000);
                if (isMongoLive)
                {
                    Invoke(new Action(() => MongoDB_Status.Text = "Povezava na voljo"));
                    Invoke(new Action(() => MongoDB_Status.ForeColor = Color.Green));
                    WriteToConsole("Povezava na MongoDB uspešna!");
                    dbClient_connection = true;
                    return true;
                }
                else
                {
                    Invoke(new Action(() => MongoDB_Status.Text = "Napaka"));
                    Invoke(new Action(() => MongoDB_Status.ForeColor = Color.Red));
                    WriteToConsole("Povezava na MongoDB neuspešna!");
                    dbClient_connection = false;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Invoke(new Action(() => MongoDB_Status.Text = "Napaka"));
                Invoke(new Action(() => MongoDB_Status.ForeColor = Color.Red));
                WriteToConsole("Napaka: " + ex.ToString());
                return false;
            }
        }
        public void WriteToConsole(string message)
        {
            try
            {
                // richTextBox2.Invoke(new MethodInvoker(delegate { Text += "[" + DateTime.Now.ToString("HH:mm:ss") + "] " + message + "\n"; }));
                Invoke(new Action(() => console.Text += "[" + DateTime.Now.ToString("HH:mm:ss") + "] " + message + "\n"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            //richTextBox2.Text += "[" + DateTime.Now.ToString("HH:mm:ss") + "] " + message + "\n";
        }

        public Nastavitve()
        {
            InitializeComponent();
        }

        private void ConnectionString_CheckedChanged(object sender, EventArgs e)
        {
            if (connectionString_checkbox.Checked) 
            {
                username_textbox.Enabled = false;
                password_textbox.Enabled = false;
                ipAddressMongoDB_textbox.Enabled = false;
                portMongoDB_textbox.Enabled = false;
                connectionString_textbox.Enabled = true;
                Properties.Settings.Default.MongoDB_connectionStringYesNo = true;
            }
            else
            {
                username_textbox.Enabled = true;
                password_textbox.Enabled = true;
                ipAddressMongoDB_textbox.Enabled = true;
                portMongoDB_textbox.Enabled = true;
                connectionString_textbox.Enabled = false;
                Properties.Settings.Default.MongoDB_connectionStringYesNo = false;
            }
        }

        private void Nastavitve_Load(object sender, EventArgs e)
        {
            connectionString_textbox.Enabled = false;

            connectionString_textbox.Text = Properties.Settings.Default.MongoDB_connectionString;
            username_textbox.Text = Properties.Settings.Default.MongoDB_userName;
            password_textbox.Text = Properties.Settings.Default.MongoDB_password;
            ipAddressMongoDB_textbox.Text = Properties.Settings.Default.MongoDB_IPaddress;
            portMongoDB_textbox.Text = Properties.Settings.Default.MongoDB_port;
            database_textbox.Text = Properties.Settings.Default.MongoDB_database;
            collection_textbox.Text = Properties.Settings.Default.MongoDB_collection;
            //Properties.Settings.Default.MongoDB_uporabaNiza ? nizZaPovezavo_checkbox.checked = true :  nizZaPovezavo_checkbox.Checked = false;
            if (Properties.Settings.Default.MongoDB_connectionStringYesNo == true)
                connectionString_checkbox.Checked = true;
            else
                connectionString_checkbox.Checked = false;
            //nizZaPovezavo_checkbox.Checked = true;

            ipAddressPredictionIO_textbox.Text = Properties.Settings.Default.PredictionIO_IPaddress;
            portPredictionIOEventServer_textbox.Text = Properties.Settings.Default.PredictionIO_eventServer;
            portPredictionIOEngineServer_textbox.Text = Properties.Settings.Default.PredictionIO_engineServer;
            predictionIOAccessKey_textbox.Text = Properties.Settings.Default.PredictionIO_accessKey;




        }

        private void MongoDBConnectionTest_button_Click(object sender, EventArgs e)
        {
            if (connectionString_checkbox.Checked == true) //če uporabimo niz za povezavo
            {
                Thread preizkusPovezave = new Thread(() =>
                {

                    PreizkusPovezaveMongoDB(connectionString_textbox.Text, database_textbox.Text, collection_textbox.Text);

                });
                preizkusPovezave.Start();
            }
            else if (connectionString_checkbox.Checked == false) // če uporabimo posamezne polja za ustvarjanje niza za povezavo
            {
                Thread preizkusPovezave = new Thread(() =>
                {
                    string niz = "mongodb://" + username_textbox.Text + ":" + password_textbox.Text + "@" + ipAddressMongoDB_textbox.Text + ":" + portMongoDB_textbox.Text + "/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";
                    WriteToConsole("Preizkus niza: " + niz);
                    PreizkusPovezaveMongoDB(niz, database_textbox.Text, collection_textbox.Text);
                });
                preizkusPovezave.Start();
                //string niz = "mongodb://" + Properties.Settings.Default.MongoDB_uporabniskoIme + ":" + Properties.Settings.Default.MongoDB_geslo + "@" + Properties.Settings.Default.MongoDB_IPnaslov + ":" + Properties.Settings.Default.MongoDB_vrata + "/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";
            }
        }

        private void MongoDBsaveConnection_button_Click(object sender, EventArgs e)
        {
            if (connectionString_checkbox.Checked == true) //če je niz za povezavo okence obkljukano shranimo niz, bazo podatkov in zbirko
            {
                Properties.Settings.Default.MongoDB_connectionString = connectionString_textbox.Text;
                Properties.Settings.Default.MongoDB_database = database_textbox.Text;
                Properties.Settings.Default.MongoDB_collection = collection_textbox.Text;
                Properties.Settings.Default.MongoDB_connectionStringYesNo = true;
            }
            else if (connectionString_checkbox.Checked == false)
            {
                Properties.Settings.Default.MongoDB_userName = username_textbox.Text;
                Properties.Settings.Default.MongoDB_password = password_textbox.Text;
                Properties.Settings.Default.MongoDB_IPaddress = ipAddressMongoDB_textbox.Text;
                Properties.Settings.Default.MongoDB_port = portMongoDB_textbox.Text;
                Properties.Settings.Default.MongoDB_database = database_textbox.Text;
                Properties.Settings.Default.MongoDB_collection = collection_textbox.Text;
                Properties.Settings.Default.MongoDB_connectionStringYesNo = false;
            }
        }


        private void PredictionIOConnectionTest_button_Click(object sender, EventArgs e)
        {
            Thread preizkusPovezaveStreznik = new Thread(() =>
            {
                Invoke(new Action(() => predictionIOConnectionTest_button.Enabled = false));
                Invoke(new Action(() => predictionIOsaveConnection_button.Enabled = false));

                PredictionIO_serverConnection(ipAddressPredictionIO_textbox.Text, portPredictionIOEventServer_textbox.Text);

                Invoke(new Action(() => predictionIOConnectionTest_button.Enabled = true));
                Invoke(new Action(() => predictionIOsaveConnection_button.Enabled = true));

            });
            preizkusPovezaveStreznik.Start();

            Thread preizkusPovezaveAPI = new Thread(() =>
            {

                PredictionIO_engineConnection(ipAddressPredictionIO_textbox.Text, portPredictionIOEngineServer_textbox.Text);

            });
            preizkusPovezaveAPI.Start();

        }

        private void predictionIOsaveConnection_button_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.PredictionIO_IPaddress = ipAddressPredictionIO_textbox.Text;
            Properties.Settings.Default.PredictionIO_eventServer = portPredictionIOEventServer_textbox.Text;
            Properties.Settings.Default.PredictionIO_engineServer = portPredictionIOEngineServer_textbox.Text;
            Properties.Settings.Default.PredictionIO_accessKey = predictionIOAccessKey_textbox.Text;
        }
    }
}
