﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aplikacija
{
    class ClientID
    {
        private string ID;
        private Dictionary<string, int> Keywords;

        public ClientID()
        {
            ID = "";
            Keywords = new Dictionary<string, int>();
        }
        public ClientID(string _ID)
        {
            ID = _ID;
            Keywords = new Dictionary<string, int>();
        }

        public string getID() { return ID; }
        public void setID(string _ID) { ID = _ID; }

        public Dictionary<string, int> getKeywords() { return Keywords; }
        public void setKeywords(Dictionary<string, int> _Keywords) { Keywords = _Keywords; }
        public void changeCounter(string name, int number)
        {
            if(Keywords.ContainsKey(name)) //če obstaja že ta keyword v dictionary
            {
                Keywords[name] = number;
            }else
            {
                Console.WriteLine("This should not happend, something is wrong -> " + name);
                Keywords.Add(name, 1);
            }
        }
        public void initializeKey(string key)
        {
            if (!Keywords.ContainsKey(key)) //če obstaja že ta keyword v dictionary
            {
                Keywords.Add(key, 0); //tukaj samo nastavimo vse keyworde
            }
            else
            {
                Console.WriteLine("This should not happend, key already exists! -> " + key); //ignoring
            }
        }

    }
}
