﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aplikacija
{
    class MediaID
    {
        private string ID, Channel, Language, Title, Type, ChannelGenre;
        private List<string> ChannelKeywords;
        public MediaID()
        {
            ID = "";
            Channel = "";
            Language = "";
            Title = "";
            Type = "";
            ChannelGenre = "";
            ChannelKeywords = new List<string>();
        }

        public MediaID(string _ID, string _Channel, string _Language, string _Title, string _Type, string _ChannelGenre, List<string> _ChannelKeywords)
        {
            ID = _ID;
            Channel = _Channel;
            Language = _Language;
            Title = _Title;
            Type = _Type;
            ChannelGenre = _ChannelGenre;
            ChannelKeywords = _ChannelKeywords;
        }

        public string getID() { return ID; }
        public void setID(string _ID) { ID = _ID; }
        public string getChannel() { return Channel; }
        public void setChannel(string _Channel) { Channel = _Channel; }
        public string getLanguage() { return Language; }
        public void setLanguage(string _Language) { Language = _Language; }
        public string getTitle() { return Title; }
        public void setTitle(string _Title) { Title = _Title; }
        public string getType() { return Type; }
        public void setType(string _Type) { Type = _Type; }
        public string getChannelGenre() { return ChannelGenre; }
        public void setChannelGenre(string _ChannelGenre) { ChannelGenre = _ChannelGenre; }
        public List<string> getChannelKeywords() { return ChannelKeywords; }
        public void setChannelKeywords(List<string> _ChannelKeywords) { ChannelKeywords = _ChannelKeywords; }
        public void setChannelKeywordsOne(string _keyword) { ChannelKeywords.Add(_keyword); }
        public void clear()
        {
            ID = "";
            Channel = "";
            Language = "";
            Title = "";
            Type = "";
            ChannelGenre = "";
            ChannelKeywords.Clear();
        }
    }
}
