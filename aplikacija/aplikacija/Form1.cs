﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using RestSharp;

namespace aplikacija
{



    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        MongoClient dbClient;
        bool dbClient_connection = false;
        List<ClientID> listOfClients;
        string filePath = string.Empty;
        List<RecommendatioScore> ClientID_Recommendation = new List<RecommendatioScore>();
        Advertisements advertisementsForRecommendation = new Advertisements();
        Advertisements advertisements = new Advertisements();
        Dictionary<String, double> recommendations = new Dictionary<string, double>();
        

        public void WriteToConsole(string message)
        {
            try
            {
                Invoke(new Action(() => console.Text += "[" + DateTime.Now.ToString("HH:mm:ss") + "] " + message + "\n"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        public bool ConnectToMongoDB(string connection_string, string dataBase)
        {
            WriteToConsole("Poizkus vzpostavljanje povezave v MongoDB podatkovni strežnik");
            Invoke(new Action(() => MongoDB_Status.Text = "Povezujem.."));
            Invoke(new Action(() => MongoDB_Status.ForeColor = Color.Orange));
            try
            {
                dbClient = new MongoClient(connection_string);
                var database = dbClient.GetDatabase(dataBase);
                bool isMongoLive = database.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(1000); //pošljemo ping zahtevo na strežnik
                if (isMongoLive)
                {
                    Invoke(new Action(() => MongoDB_Status.Text = "Povezan"));
                    Invoke(new Action(() => MongoDB_Status.ForeColor = Color.Green));
                    WriteToConsole("Povezan v MongoDB podatkovni strežnik!");
                    dbClient_connection = true;
                    return true;
                }
                else
                {
                    Invoke(new Action(() => MongoDB_Status.Text = "Napaka"));
                    Invoke(new Action(() => MongoDB_Status.ForeColor = Color.Red));
                    WriteToConsole("Neznana napaka. Povezava v MongoDB podatkovni strežnik neuspešna");
                    dbClient_connection = false;
                    return false;
                }
            }
            catch (Exception ex)
            {
                Invoke(new Action(() => MongoDB_Status.Text = "Napaka"));
                Invoke(new Action(() => MongoDB_Status.ForeColor = Color.Red));
                WriteToConsole("Neznana napaka: " + ex.ToString());
                return false;
            }
        }

        public bool CheckStatusPredictionIO(string ip, string port)
        {
            using (var httpClient = new HttpClient())
            {
                WriteToConsole("Preizkus povezave v PredictionIO event strežnika");
                Invoke(new Action(() => PredictionIO_Status.Text = "Poizkus.."));
                Invoke(new Action(() => PredictionIO_Status.ForeColor = Color.Orange));
                string path = "http://" + ip + ":" + port;
                try
                {
                    using (var request = new HttpRequestMessage(new HttpMethod("GET"), path)) //preverjamo samo, če dobimo odgovor ali pa ne
                    {
                        var response = httpClient.SendAsync(request).Result;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = "Napaka: " + ex.ToString();
                    Invoke(new Action(() => PredictionIO_Status.Text = "Napaka"));
                    Invoke(new Action(() => PredictionIO_Status.ForeColor = Color.Red));
                    WriteToConsole(errorMessage);
                    return false;
                }
                Invoke(new Action(() => PredictionIO_Status.Text = "Dostopno"));
                Invoke(new Action(() => PredictionIO_Status.ForeColor = Color.Green));

                WriteToConsole("Povezava do PredictionIO event strežnika za podatke je dostopna");
                return true;

            }
        }

        public bool CheckStatusPredictionIOEngine(string ip, string port)
        {
            using (var httpClient = new HttpClient())
            {

                WriteToConsole("Preizkus povezave v PredictionIO engine strežnik");
                Invoke(new Action(() => PredictionIOEngine_Status.Text = "Poizkus.."));
                Invoke(new Action(() => PredictionIOEngine_Status.ForeColor = Color.Orange));

                string path = "http://" + ip + ":" + port;
                try
                {
                    using (var request = new HttpRequestMessage(new HttpMethod("GET"), path))
                    {
                        var response = httpClient.SendAsync(request).Result;
                    }
                }
                catch (Exception ex)
                {
                    string errorMessage = "Napaka: " + ex.ToString();
                    Invoke(new Action(() => PredictionIOEngine_Status.Text = "Napaka"));
                    Invoke(new Action(() => PredictionIOEngine_Status.ForeColor = Color.Red));
                    WriteToConsole(errorMessage);
                    return false;
                }
                Invoke(new Action(() => PredictionIOEngine_Status.Text = "Dostopno"));
                Invoke(new Action(() => PredictionIOEngine_Status.ForeColor = Color.Green));

                WriteToConsole("Povezava do PredictionIO engine strežnika je dostopna");
                return true;

            }

        }

        private void CheckConnections()
        {
            Thread t1 = new Thread(() =>
            {
                CheckStatusPredictionIO(Properties.Settings.Default.PredictionIO_IPaddress, Properties.Settings.Default.PredictionIO_eventServer);
            });
            t1.Start();

            Thread t2 = new Thread(() =>
            {
                CheckStatusPredictionIOEngine(Properties.Settings.Default.PredictionIO_IPaddress, Properties.Settings.Default.PredictionIO_engineServer);
            });
            t2.Start();

            Thread t3 = new Thread(() =>
            {
                if (Properties.Settings.Default.MongoDB_connectionStringYesNo == true)
                {
                    ConnectToMongoDB(Properties.Settings.Default.MongoDB_connectionString, Properties.Settings.Default.MongoDB_database); //če uporabimo connection string
                }
                else
                {
                    string connectionString = "mongodb://" + Properties.Settings.Default.MongoDB_userName + ":" + Properties.Settings.Default.MongoDB_password + "@" + Properties.Settings.Default.MongoDB_IPaddress + ":" + Properties.Settings.Default.MongoDB_port + "/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false";
                    WriteToConsole("Preizkus niza: " + connectionString);
                    ConnectToMongoDB(connectionString, Properties.Settings.Default.MongoDB_database);
                }
            });
            t3.Start();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            Properties.Settings.Default.MongoDB_connectionString = "mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false";
            //"mongodb://USERNAME:PASSWORD@IP_ADDRESS:GATES/?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false"
            Properties.Settings.Default.MongoDB_userName = "VIP";
            Properties.Settings.Default.MongoDB_password = "naloga";
            Properties.Settings.Default.MongoDB_IPaddress = "3.124.117.234";
            Properties.Settings.Default.MongoDB_port = "27017";
            Properties.Settings.Default.MongoDB_database = "local";
            Properties.Settings.Default.MongoDB_collection = "all_channels";
            Properties.Settings.Default.MongoDB_connectionStringYesNo = true;

            Properties.Settings.Default.PredictionIO_IPaddress = "192.168.153.130";
            Properties.Settings.Default.PredictionIO_engineServer = "8000";
            Properties.Settings.Default.PredictionIO_eventServer = "7070";
            Properties.Settings.Default.PredictionIO_accessKey = "yXud7YMCVvUkbFRTKQZot9Lu1s0Q0MxQDx3rWMqb42FVS95XCstPmuET5F3Kzl34";

            Properties.Settings.Default.MediaID_file = "C:\\Users\\Leon\\Desktop\\logs_updated\\mediaid_data_sample.xml";
            Properties.Settings.Default.LogFile = "C:\\Users\\Leon\\Desktop\\logs_updated\\nove.xml";
            Properties.Settings.Default.OutputFile = "C:\\Users\\Leon\\Desktop\\logs_updated\\nove.json";

            Properties.Settings.Default.LogFiles = new System.Collections.Specialized.StringCollection();

            mediaID_textbox.Text = Properties.Settings.Default.MediaID_file;
            log_textbox.Text = Properties.Settings.Default.LogFile;
            json_textbox.Text = Properties.Settings.Default.OutputFile;
            numberOfLogs_checkbox.Checked = true;

            openAdvertisements_textbox.Text = "C:\\Users\\Leon\\Desktop\\oglasi\\dokument.txt";
            filePath = openAdvertisements_textbox.Text;

            deleteAdvertisement_button.Enabled = false;

            listOfClients = new List<ClientID>();

            CheckConnections();
        }


        private void UploadButton_Click(object sender, EventArgs e)
        {
            List<MediaID> listOfMediaID = new List<MediaID>();

            XmlDocument documentForMediaID = new XmlDocument();
            documentForMediaID.Load(Properties.Settings.Default.MediaID_file);
            XmlElement rootOfXml = documentForMediaID.DocumentElement;
            XmlNodeList xmlNodes = rootOfXml.SelectNodes("/Events/Event");

            foreach (XmlNode xndNode in xmlNodes) //dodamo vse podatke od vseh kanalov v MediaID podatkovno strukturo
            {
                MediaID tmp = new MediaID();
                tmp.setID(xndNode["MediaID"].InnerText);
                tmp.setChannel(xndNode["Channel"].InnerText);
                tmp.setLanguage(xndNode["Language"].InnerText);
                tmp.setTitle(xndNode["Title"].InnerText);
                tmp.setType(xndNode["Type"].InnerText);
                tmp.setChannelGenre(xndNode["Genre"].InnerText);
                foreach (XmlNode xndNodeKeywords in xndNode["Keywords"].ChildNodes)
                {
                    tmp.setChannelKeywordsOne(xndNodeKeywords.InnerText);
                }
                listOfMediaID.Add(tmp);


            }

            XmlDocument documentForLogs = new XmlDocument();

            if (numberOfLogs_checkbox.Checked == true) //če izberemo samo eno datoteko za nalaganje v MongoDB podatkovno bazo
            {
                documentForLogs.Load(Properties.Settings.Default.LogFile);
                XmlElement rootOfXmlLogs = documentForLogs.DocumentElement;
                XmlNodeList xmlNodesLogs = rootOfXmlLogs.SelectNodes("/Watchlist/Event");

                List<Log> listOfLogs = new List<Log>();

                //najprej dodamo vsak dnevniški vnos v podatkovno strukturo 
                foreach (XmlNode xndNode in xmlNodesLogs)
                {
                    List<string> Keywords = new List<string>();
                    for (int i = 0; i < listOfMediaID.Count(); i++)
                    {
                        if (listOfMediaID[i].getID() == xndNode["MediaID"].InnerText)
                        {
                            Keywords = (listOfMediaID[i].getChannelKeywords());
                            break;
                        }

                    }
                    listOfLogs.Add(new Log(xndNode["Timestamp"].InnerText, xndNode["ClientID"].InnerText, xndNode["MediaID"].InnerText, Keywords));
                }

                string jsonString = JsonConvert.SerializeObject(listOfLogs);
                listOfLogs.Clear();

                //brez vmesnega shranjevanja je bila napaka pri pretvarjanju

                File.WriteAllText(Properties.Settings.Default.OutputFile, jsonString); //zapišemo dobljeno json obliko v datoteko

                jsonString = string.Empty; //izbrišemo vse podatke iz spremenljivke, da sprostimo pomnilnik
                string text = File.ReadAllText(Properties.Settings.Default.OutputFile); //preberemo datoteko, katero smo prej shranili

                var document = BsonSerializer.Deserialize<IList<BsonDocument>>(text); //to besedilo deserializiramo v Bson dokument (format, ki ga podpira MongoDB podatkovna baza)
                text = string.Empty; //ponastavimo spremenljivko
                var watch = Stopwatch.StartNew(); //ustvarimo štoparico za merjenje časa nalaganja 

                var database = dbClient.GetDatabase(Properties.Settings.Default.MongoDB_database);
                var collection = database.GetCollection<BsonDocument>(Properties.Settings.Default.MongoDB_collection);

                collection.InsertMany(document); //vnesemo celotni dokument
                                                 //collection.InsertMany(document);

                WriteToConsole("Dokument " + Properties.Settings.Default.LogFile + " uspešno naložen! Čas izvajanja: " + (watch.ElapsedMilliseconds / 1000) + "ms");
                Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds / 1000} ms");

            }else
            {
                int numberOfFiles = Properties.Settings.Default.LogFiles.Count;
                List<Log> listOfLogs = new List<Log>();
                for (int i = 0; i < numberOfFiles; i++)
                {
                    documentForLogs.Load(Properties.Settings.Default.LogFiles[i]);
                    XmlElement rootOfXmlLogs = documentForLogs.DocumentElement;
                    XmlNodeList xmlNodesLogs = rootOfXmlLogs.SelectNodes("/Watchlist/Event");
                    string test = Properties.Settings.Default.LogFiles[i];
                    listOfLogs.Clear();

                    //najprej dodamo vsak dnevniški vnos v podatkovno strukturo 
                    foreach (XmlNode xndNode in xmlNodesLogs)
                    {
                        List<string> Keywords = new List<string>();
                        for (int j = 0; j < listOfMediaID.Count(); j++)
                        {
                            if (listOfMediaID[j].getID() == xndNode["MediaID"].InnerText)
                            {
                                Keywords = (listOfMediaID[j].getChannelKeywords());
                                break;
                            }

                        }
                        listOfLogs.Add(new Log(xndNode["Timestamp"].InnerText, xndNode["ClientID"].InnerText, xndNode["MediaID"].InnerText, Keywords));
                    }

                    string jsonString = JsonConvert.SerializeObject(listOfLogs);
                    listOfLogs.Clear();

                    //brez vmesnega shranjevanja je bila napaka pri pretvarjanju
                    File.WriteAllText(Properties.Settings.Default.OutputFile, string.Empty); //izbrišemo karkoli je v začasni datoteki
                    File.WriteAllText(Properties.Settings.Default.OutputFile, jsonString); //zapišemo dobljeno json obliko v datoteko

                    jsonString = string.Empty; //izbrišemo vse podatke iz spremenljivke, da sprostimo pomnilnik
                    string text = File.ReadAllText(Properties.Settings.Default.OutputFile); //preberemo datoteko, katero smo prej shranili
                    File.WriteAllText(Properties.Settings.Default.OutputFile, string.Empty); //ponovno izbrišemo datoteko

                    var document = BsonSerializer.Deserialize<IList<BsonDocument>>(text); //to besedilo deserializiramo v Bson dokument (format, ki ga podpira MongoDB podatkovna baza)
                    text = string.Empty; //ponastavimo spremenljivko
                    var watch = Stopwatch.StartNew(); //ustvarimo štoparico za merjenje časa nalaganja 
                    var database = dbClient.GetDatabase(Properties.Settings.Default.MongoDB_database);
                    var collection = database.GetCollection<BsonDocument>(Properties.Settings.Default.MongoDB_collection);

                    collection.InsertMany(document); //vnesemo celotni dokument
                                                     //collection.InsertMany(document);

                    WriteToConsole("Dokument " + Properties.Settings.Default.LogFile + " uspešno naložen! Čas izvajanja: " + (watch.ElapsedMilliseconds / 1000) + "ms. Ostalo še " + i + "/" + numberOfFiles + "datotek");
                    Console.WriteLine($"Execution Time: {watch.ElapsedMilliseconds / 1000} ms");
                    document.Clear();
                }
            }
        }

        private void GetUsers_Click(object sender, EventArgs e)
        {
            users_listbox.Items.Clear();
            int numberOfRecomendations = 10; //iz priporočil dobimo 10 priporočil za enega uporabnika


            //za preizkus uporabimo 10 istih uporabnikov, da lahko primerjamo rezultat
            List<string> clients = new List<string>();
            clients.Add("00021813-3392-5da6-91f4-15df6ab162e2");
            clients.Add("00066c38-c55f-5954-9064-2f53fd8a1bc5");
            clients.Add("00036953-dd59-57bd-b922-7668e0d683cc");
            clients.Add("00037b79-2c90-585a-8046-8e8a4c5a52d5");
            clients.Add("000517e2-cdda-54e3-b414-0981aa0ebca6");
            clients.Add("0006101e-037d-533a-b303-3b0cec97c159");
            clients.Add("00066d75-a1fd-5086-a5d3-f44c22519d16");
            clients.Add("00070789-cdab-50a7-8899-4a1f704bc6b4");
            clients.Add("000823bc-0c2f-5135-9c69-5a1bbf7a2d44");
            clients.Add("000af792-9b2d-5318-87e9-61946d97dfec");

            //ta thread poišče recommendation od predictionio
            Thread t3 = new Thread(() =>
            {
                try
                {
                    using (var httpClient = new HttpClient())
                    {
                        string connectionString = "http://" + Properties.Settings.Default.PredictionIO_IPaddress + ":" + Properties.Settings.Default.PredictionIO_engineServer + "/queries.json";
                        for (int i = 0; i < clients.Count(); i++) //za vsakega uporabnika pošljemo zahtevo na PredictionIO strežnik
                        {
                            using (var request = new HttpRequestMessage(new HttpMethod("POST"), connectionString))
                            {

                                request.Content = new StringContent("{ \"user\": \"" + clients[i] + "\", \"num\": " + numberOfRecomendations + "}"); //vnesemo uporabnika in število priporočil, ki jih želimo dobiti nazaj
                                request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json"); //nastavimo glavo http zahteve

                                var response = httpClient.SendAsync(request).Result; //dobimo odgovor od strežnika
                                string data = response.Content.ReadAsStringAsync().Result; //pretvorimo ta odgovor v string (dobimo v JSON obliki)
                                RootObject obj = JsonConvert.DeserializeObject<RootObject>(data); //zaradi deserializacije je ustvarjen novi razred, s pomočjo katerega pretvorimo v željeno podatkovno strukturo

                                Dictionary<string, double> temp_recommendation = new Dictionary<string, double>(); //ime in vrednost vsakega priporočila
                                foreach (var x in obj.itemScores)
                                {
                                    temp_recommendation.Add(x.item, x.score); //dodamo vsa priporočila v podatkovno strukturo
                                }
                                ClientID_Recommendation.Add(new RecommendatioScore(clients[i], temp_recommendation)); //dodamo novega uporabnika ter njegova priporočila 
                                Invoke(new Action(() => users_listbox.Items.Add(clients[i]))); //dodamo njegov ID v listbox
                                //listBox1.Items.Add(clients[i]);    
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteToConsole(ex.ToString());
                }
            });
            t3.Start();

            //ta thread poišče in primerja iz predictionio engine serverja in mongodb prvotne rezultate
            Thread t4 = new Thread(() =>
            {
                try
                {
                    if (dbClient_connection)
                    {
                        var database = dbClient.GetDatabase(Properties.Settings.Default.MongoDB_database);
                        var collection = database.GetCollection<BsonDocument>(Properties.Settings.Default.MongoDB_collection);
                        List<string> tempKeywords = new List<string>();

                        for (int i = 0; i < clients.Count(); i++)
                        {
                            listOfClients.Add(new ClientID(clients[i]));
                        }
                        for (int i = 0; i < clients.Count(); i++)
                        {
                            var tempFilter = Builders<BsonDocument>.Filter.Eq("ClientID", clients[i]);
                            var getAllKeywordsForOneUser = collection.Distinct<string>("Keywords", tempFilter).ToList();

                            foreach (var x in getAllKeywordsForOneUser) //dodamo vse keyworde k enemu uporabniku
                                {
                                listOfClients[i].initializeKey(x);
                                tempKeywords.Add(x);
                            }

                            foreach (var x in tempKeywords) //preštejemo kolikokrat se je keyword pojavil
                                {
                                var aggregate = collection.Aggregate()
                                            .Match(new BsonDocument { { "ClientID", listOfClients[i].getID() }, { "Keywords", new BsonDocument("$eq", x) } })
                                            .Count();
                                var results = aggregate.ToList();

                                listOfClients[i].changeCounter(x, Convert.ToInt32(results[0].Count));
                            }
                            tempKeywords.Clear();
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteToConsole(ex.ToString());
                }
            });
            t4.Start();
        }

        
        private void Users_DoubleClick(object sender, EventArgs e)
        {
            recommendedAdvertisements_listview.Clear();
            //izbrišemo vse vnose v grafe ter izbrišemo naslove
            foreach (var series in predictionNumbers_chart.Series)
            {
                series.Points.Clear();
            }
            foreach (var series in predictionPercentage_chart.Series)
            {
                series.Points.Clear();
            }
            predictionNumbers_chart.Titles.Clear();
            predictionPercentage_chart.Titles.Clear();


            var selectedItem = users_listbox.SelectedItem; //zapomnimo si izbranega uporabnika
            RecommendatioScore oneUser = ClientID_Recommendation.Find(x => x.getID() == selectedItem); //poiščemo tega uporabnika iz podatkovne strukture, ter ga vpišemo v novo spremenljivko
            ClientID oneUser_ClientID = listOfClients.Find(x => x.getID() == selectedItem); //poiščemo še tega uporabnika iz podatkovne strukture, ki smo jo pridobili iz podatkovne baze

            double sumOfScores = 0;
            foreach (var x in oneUser.getScores())
            {
                sumOfScores += x.Value; //za procente potrebujemo skupni seštevek vseh vrednosti (vrednosti, ki smo jih dobili kot priporočila iz PredictionIO sistema
            }

            int index; //spremenljivka, s pomočjo katere sledimo, na katerem stolpcu trenutno smo
            foreach (var x in oneUser.getScores())
            {
                if (oneUser_ClientID.getKeywords().ContainsKey(x.Key)) //če obstaja ta zvrst v podatkovni bazi in predictionio bazi, spremenimo barvo in dodamo na graf
                {
                    //dodamo vrednosti za en stolpec ter spremenimo barvo v rdeče, ker se ta vrednost ujema z vrednostjo iz podatkovne baze
                    index = predictionNumbers_chart.Series["Prediction"].Points.AddXY(x.Key, x.Value);
                    predictionNumbers_chart.Series["Prediction"].Points[index].Color = System.Drawing.Color.Red;

                    //dodamo vrednosti v drugi stolpec, kjer so prikazani procenti
                    predictionPercentage_chart.Series["Prediction"].Points.AddXY(x.Key, x.Value / sumOfScores * 100);
                    predictionPercentage_chart.Series["Prediction"].Points[index].Color = System.Drawing.Color.Red;
                }
                else
                {
                    index = predictionNumbers_chart.Series["Prediction"].Points.AddXY(x.Key, x.Value);
                    predictionNumbers_chart.Series["Prediction"].Points[index].Color = System.Drawing.Color.Blue;


                    predictionPercentage_chart.Series["Prediction"].Points.AddXY(x.Key, x.Value / sumOfScores * 100);
                    predictionPercentage_chart.Series["Prediction"].Points[index].Color = System.Drawing.Color.Blue;
                }

                //vsakemu stolpcu pa še napišemo točno vrednost
                predictionNumbers_chart.Series["Prediction"].Points[index].Label = x.Value.ToString("0.###");
                predictionPercentage_chart.Series["Prediction"].Points[index].Label = (x.Value / sumOfScores).ToString("0.##") + "%";

            }

            //nastavimo, da se prikaže ime vsake vrednosti ter dodamo naslove
            predictionNumbers_chart.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            predictionNumbers_chart.Titles.Add(selectedItem.ToString());

            predictionPercentage_chart.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            predictionPercentage_chart.Titles.Add(selectedItem.ToString());
            //ListBoxItem selected_item = (ListBoxItem)

            //
            if (advertisements.Advertisement.Count == 0)
            {
                WriteToConsole("Ni naloženih oglasov.");
                return;
            }

            numberOfMatches_listview.Items.Clear();
            int steviloIteracij = Int32.Parse(rangeOfRecommendation_textbox.Text);
            var tmpKeywords = oneUser.getScores();

            int stevecUspesnosti = 0;
            Advertisements zacasniOglasi = new Advertisements();
            zacasniOglasi = advertisements;
            advertisementsForRecommendation.Advertisement.Clear();
            string message = "";
            Dictionary<Advertisement, int> steviloUjemanj = new Dictionary<Advertisement, int>();

            for (int j = 0; j < zacasniOglasi.Advertisement.Count; j++) //gremo skos vse oglase
            {
                for (int z = 0; z < zacasniOglasi.Advertisement[j].Keywords.Count; z++) //gremo skozi vsaki keyword posebej pri oglasu
                {
                    for (int k = 0; k < steviloIteracij; k++) //primerjamo keyword z oglasi
                    {
                        if (zacasniOglasi.Advertisement[j].Keywords[z].Contains(tmpKeywords.Keys.ElementAt(k))) //če se ujemata dva keyworda
                        {
                            stevecUspesnosti += 1;
                            break;
                        }
                    }
                }

                if (stevecUspesnosti != 0)
                {
                    steviloUjemanj.Add(zacasniOglasi.Advertisement[j], stevecUspesnosti);

                    stevecUspesnosti = 0;
                }

            }
            var myList = steviloUjemanj.ToList();

            myList.Sort((pair1, pair2) => pair2.Value.CompareTo(pair1.Value)); //sortiramo po tem, kolikokrat se pojavi
            foreach (var x in myList) //izpis ujemanj
            {
                message = "Oglas " + x.Key.Name + " je bil dodan z ujemanji " + x.Value;
                numberOfMatches_listview.View = View.List;
                numberOfMatches_listview.Items.Add(x.Key.Name + " " + x.Value);
                WriteToConsole(message);
            }
            RefreshDataInListView2(myList);
        }

        private void RefreshDataInListView2(List<KeyValuePair<Advertisement, int>> myList)
        {
            advertisementsForRecommendation.Advertisement.Clear();
            recommendedAdvertisements_listview.Items.Clear();
            ImageList il = new ImageList();
            for (int i = 0; i < myList.Count; i++)
            {
                advertisementsForRecommendation.Advertisement.Add(myList[i].Key);
                il.Images.Add(myList[i].Key.Name, Image.FromFile(myList[i].Key.Path_to_picture));
                //il.Images.Add(oglasiZaPriporocilo.oglasi[i].Name, Image.FromFile(oglasiZaPriporocilo.oglasi[i].Path_to_picture));
            }
            //il.Images.Add("test1", Image.FromFile(@"C:\Users\Leon\Desktop\prenos.png"));
            // il.Images.Add("test1", Image.FromFile(@"C:\Users\Leon\Desktop\fotografija.jpg"));
            il.ImageSize = new Size(140, 80);

            // listView1.View = View.LargeIcon;
            recommendedAdvertisements_listview.View = View.Tile;
            recommendedAdvertisements_listview.TileSize = new Size(recommendedAdvertisements_listview.ClientSize.Width, 100);

            //listView1.TileSize = new Size(550, 250);
            recommendedAdvertisements_listview.LargeImageList = il;
            // listView1.Items.Add("test");

            for (int i = 0; i < il.Images.Count; i++)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageIndex = i;

                lvi.Text = myList[i].Key.Description;

                recommendedAdvertisements_listview.Items.Add(lvi);
            }

        }


        private void Settings_Click(object sender, EventArgs e)
        {
            Nastavitve n = new Nastavitve();
            n.ShowDialog();
        }

       

        public void RefreshDataInListView()
        {
            advertisement_listview.Items.Clear();
            ImageList il = new ImageList();
            for (int i = 0; i < advertisements.Advertisement.Count(); i++) //dodamo vse slike v ImageList
            {
                il.Images.Add(advertisements.Advertisement[i].Name, Image.FromFile(advertisements.Advertisement[i].Path_to_picture));
            }

            il.ImageSize = new Size(140, 80);

            advertisement_listview.View = View.Tile;
            advertisement_listview.TileSize = new Size(advertisement_listview.ClientSize.Width, 100);

            advertisement_listview.LargeImageList = il;

            for (int i = 0; i < il.Images.Count; i++) //dodamo vse objekte v listView
            {
                ListViewItem lvi = new ListViewItem();
                lvi.ImageIndex = i;

                lvi.Text = advertisements.Advertisement[i].Description;

                advertisement_listview.Items.Add(lvi);
            }
        }
 
        private void RecommendedAdvertisements_DoubleClick(object sender, EventArgs e) //za izpis podatkov od določenega oglasa pri priporočilu (izpis ključnih besed)
        {
            keywordsFromAdvertisement_listbox.Items.Clear();
            //checkedListBox2.Items.Clear();
            Advertisement trenutniOglas = new Advertisement();
            if (recommendedAdvertisements_listview.SelectedItems.Count >= 1)
            {
                ListViewItem item = recommendedAdvertisements_listview.SelectedItems[0];
                for (int i = 0; i < advertisementsForRecommendation.Advertisement.Count; i++)
                {
                    if (advertisementsForRecommendation.Advertisement[i].Description == item.Text)
                    {
                        trenutniOglas = advertisementsForRecommendation.Advertisement[i];
                    }
                }

                for (int i = 0; i < trenutniOglas.Keywords.Count(); i++)
                {
                    keywordsFromAdvertisement_listbox.Items.Add(trenutniOglas.Keywords[i]);
                }

            }
        }

        private void CheckConnection_Click(object sender, EventArgs e)
        {
            CheckConnections();
        }

        Advertisement trenutniOglas = new Advertisement();
        private void Advertisements_DoubleClick(object sender, EventArgs e) //prikaz slike, ključnih besed in kratkega opisa pri izbiri določenega oglasa
        {
            keywords_listbox2.Items.Clear();
            //checkedListBox2.Items.Clear();
            deleteAdvertisement_button.Enabled = true;
            if (advertisement_listview.SelectedItems.Count >= 1)
            {
                ListViewItem item = advertisement_listview.SelectedItems[0];
                for (int i = 0; i < advertisements.Advertisement.Count; i++)
                {
                    if (advertisements.Advertisement[i].Description == item.Text)
                    {
                        trenutniOglas = advertisements.Advertisement[i];
                    }
                }
                Image img = Image.FromFile(trenutniOglas.Path_to_picture);
                pictureOfAdvertisement_picturebox.Image = img;
                pictureOfAdvertisement_picturebox.SizeMode = PictureBoxSizeMode.StretchImage;
                description_label3.Text = trenutniOglas.Description;
                for (int i = 0; i < trenutniOglas.Keywords.Count(); i++)
                {
                    keywords_listbox2.Items.Add(trenutniOglas.Keywords[i]);
                }

            }
        }

        private void NumberOFLogs_CheckedChanged(object sender, EventArgs e)
        {
            if (numberOfLogs_checkbox.Checked == true) //imamo samo eno datoteko
            {
                logs_textbox.Enabled = false;
                logs_searchButton.Enabled = false;
                logs_saveButton.Enabled = false;

                log_textbox.Enabled = true;
                log_searchButton.Enabled = true;
                log_saveButton.Enabled = true;

                json_textbox.Enabled = true;
                json_searchButton.Enabled = true;
                json_saveButton.Enabled = true;
            }
            else if (numberOfLogs_checkbox.Checked == false)
            {
                logs_textbox.Enabled = true;
                logs_searchButton.Enabled = true;
                logs_saveButton.Enabled = true;

                log_textbox.Enabled = false;
                log_searchButton.Enabled = false;
                log_saveButton.Enabled = false;

                json_textbox.Enabled = false;
                json_searchButton.Enabled = false;
                json_saveButton.Enabled = false;
            }
        }


        private void MediaID_searchButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "C:\\Users\\Leon\\Desktop\\logs_updated\\";
                openFileDialog.Filter = "XML files (*.xml)|*.xml";
                openFileDialog.FilterIndex = 0;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    mediaID_textbox.Text = openFileDialog.FileName;
                }
            }
        }

        private void MediaID_saveButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.MediaID_file = mediaID_textbox.Text;
        }

        private void Log_searchButton_Click(object sender, EventArgs e)
        {

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "C:\\Users\\Leon\\Desktop\\logs_updated\\";
                openFileDialog.Filter = "XML files (*.xml)|*.xml";
                openFileDialog.FilterIndex = 0;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    log_textbox.Text = openFileDialog.FileName;

                }
            }
        }

        private void Log_saveButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.LogFile = log_textbox.Text;
        }



        private void Json_searchButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "C:\\Users\\Leon\\Desktop\\logs_updated\\";
                openFileDialog.Filter = "JSON files (*.json)|*.json";
                openFileDialog.FilterIndex = 0;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    json_textbox.Text = openFileDialog.FileName;
                }
            }
        }

        private void Json_saveButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.OutputFile = json_textbox.Text;
        }

        private void Logs_searchButton_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "C:\\Users\\Leon\\Desktop\\logs_updated\\";
                openFileDialog.Filter = "XML files (*.xml)|*.xml";
                openFileDialog.FilterIndex = 0;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    logs_textbox.Text += openFileDialog.FileName + "\n";
                }
            }
        }

        private void Logs_saveButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.LogFiles.Clear();
            string[] RichTextBoxLines = logs_textbox.Lines;
            foreach (string line in RichTextBoxLines)
            {
                if (!string.IsNullOrEmpty(line))
                    Properties.Settings.Default.LogFiles.Add(line);
            }
        }

        private void OpenAdvertisements_button_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(openAdvertisements_textbox.Text)) //če textbox ni prazen
            {
                try
                {
                    var fileStream = openAdvertisements_textbox.Text;

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(Advertisements));
                        advertisements = (Advertisements)serializer.Deserialize(reader);
                        reader.Close();
                        RefreshDataInListView();
                    }
                }
                catch (Exception ex)
                {
                    WriteToConsole(ex.ToString());
                }
            }
        }

        private void SearchAdvertisements_button_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "C:\\Users\\Leon\\Desktop\\oglasi";
                openFileDialog.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;
                    openAdvertisements_textbox.Text = filePath;
                    //Read the contents of the file into a stream
                    var fileStream = openFileDialog.OpenFile();

                    using (StreamReader reader = new StreamReader(fileStream))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(Advertisements));
                        advertisements = (Advertisements)serializer.Deserialize(fileStream);
                        RefreshDataInListView();
                        reader.Close();

                    }
                }
            }
        }

        private void SaveAdvertisements_button_Click(object sender, EventArgs e)
        {
            XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(Advertisements));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces(); //odstranimo namespace
            ns.Add("", "");
            using (StreamWriter writer = new StreamWriter(filePath))
            {
                x.Serialize(writer, advertisements);
            }
        }

        private void SearchPicture_button_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "C:\\Users\\Leon\\Desktop\\oglasi";
                openFileDialog.Filter = "All files (*.*)|*.*";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    pathToPicture_textbox.Text = openFileDialog.FileName;
                }
            }
        }

        private void AddKeyword_button_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(keyword_textbox.Text)) //če je vpisano ime oglasa
            {
                keywords_listbox.Items.Add(keyword_textbox.Text);
                addNewAdvertisement_button.Enabled = true;
                keyword_textbox.Text = "";
            }
        }

        private void AddNewAdvertisement_button_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(name_textbox.Text)) //če je vpisano ime oglasa
                {
                    if (!string.IsNullOrWhiteSpace(description_textbox.Text)) //če je vpisani kratek opis oglasa
                    {
                        if (!string.IsNullOrWhiteSpace(pathToPicture_textbox.Text)) //če je vpisana pot do slike
                        {
                            var keywords = keywords_listbox.Items.Cast<String>().ToList();
                            advertisements.Advertisement.Add(new Advertisement(name_textbox.Text, description_textbox.Text, keywords, pathToPicture_textbox.Text));
                            name_textbox.Text = "";
                            description_textbox.Text = "";
                            pathToPicture_textbox.Text = "";
                            keyword_textbox.Text = "";
                            this.keywords_listbox.Items.Clear();
                            WriteToConsole("Oglas uspešno dodan!");
                            addNewAdvertisement_button.Enabled = false;
                            RefreshDataInListView();
                        }
                        else
                        {
                            WriteToConsole("Pot do slike ni vpisana!");
                        }
                    }
                    else
                    {
                        WriteToConsole("Krakte opis oglasa ni vpisan!");
                    }
                }
                else
                {
                    WriteToConsole("Ime oglasa ni vpisano!");
                }
            }
            catch (Exception ex)
            {
                WriteToConsole(ex.ToString());
            }
        }

        private void UploadButtonPredictionIO_Click(object sender, EventArgs e)
        {
            try
            {
                var database = dbClient.GetDatabase(Properties.Settings.Default.MongoDB_database);
                var collection = database.GetCollection<BsonDocument>(Properties.Settings.Default.MongoDB_collection);

                var emptyFilter = Builders<BsonDocument>.Filter.Empty;
                var uniqueUsers = collection.Distinct<string>("ClientID", emptyFilter).ToList();
                var numberOfUsers = Int32.Parse(numberOfUsersToUpload_textbox.Text);

                List<ClientID> temporaryListOfClients = new List<ClientID>();
                var uniqueUsersNumber = collection.Distinct<string>("ClientID", emptyFilter).ToList().Count();

                if (numberOfUsers > uniqueUsersNumber)
                    numberOfUsers = uniqueUsersNumber;

                //dodamo vse uporabnike (oziroma število, ki je vpisano)
                for (int i = 0; i < numberOfUsers; i++)
                {
                    temporaryListOfClients.Add(new ClientID(uniqueUsers[i]));
                }

                List<string> tempKeywords = new List<string>();
                for (int i = 0; i < numberOfUsers; i++)
                {
                    var tempFilter = Builders<BsonDocument>.Filter.Eq("ClientID", temporaryListOfClients[i].getID());
                    var getAllKeywordsForOneUser = collection.Distinct<string>("Keywords", tempFilter).ToList();
                    foreach (var x in getAllKeywordsForOneUser) //dodamo vse keyworde k enemu uporabniku
                    {
                        temporaryListOfClients[i].initializeKey(x);
                        tempKeywords.Add(x);
                    }
                    foreach (var x in tempKeywords) //preštejemo kolikokrat se je keyword pojavil
                    {
                        var aggregate = collection.Aggregate()
                                          .Match(new BsonDocument { { "ClientID", temporaryListOfClients[i].getID() }, { "Keywords", new BsonDocument("$eq", x) } })
                                          .Count();
                        var results = aggregate.ToList();
                        // int tempNumber = (int)results;
                        temporaryListOfClients[i].changeCounter(x, Convert.ToInt32(results[0].Count));
                    }
                    tempKeywords.Clear();
                }
                WriteToConsole("Uporabniki uspešno pridobljeni. Začenjam nalaganje na PredictionIO strežnik");
                var httpClient = new HttpClient();
                foreach (var x in temporaryListOfClients)
                {
                    string tempAddress = "http://" + Properties.Settings.Default.PredictionIO_IPaddress + ":" + Properties.Settings.Default.PredictionIO_eventServer + "/events.json?accessKey=" + Properties.Settings.Default.PredictionIO_accessKey;
                    foreach(var y in x.getKeywords()) //za vsaki keyword naložimo podatke posebej na strežnik
                    using (var request = new HttpRequestMessage(new HttpMethod("POST"), tempAddress))
                    {
                        string tmp = "{\n  \"event\" : \"rate\",\n  \"entityType\" : \"user\",\n  \"entityId\" : \"" + x.getID().ToString() + "\",\n  \"targetEntityType\" : \"item\",\n  \"targetEntityId\" : \"" + y.Key + "\",\n  \"properties\" : {\n    \"rating\" : " + y.Value + "\n } \n  }";
                        request.Content = new StringContent(tmp);
                        request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                        var response = httpClient.SendAsync(request).Result;
                    }
                }
                WriteToConsole("Uporabniki uspešno naloženi na PredictionIO strežnik");
            }
            catch (Exception ex)
            {
                WriteToConsole("Napaka med nalaganjem na PredictionIO strežnik!");
                WriteToConsole(ex.ToString());
            }
        }

        private void DeleteAdvertisement_button_Click(object sender, EventArgs e) //za brisanje oglasa
        {
            for(int i = 0; i < advertisements.Advertisement.Count; i++)
            {
                if (advertisements.Advertisement[i].Name == trenutniOglas.Name) //poiščem oglas, ki se ujema
                {
                    advertisements.RemoveAdvertisement(trenutniOglas.Name);
                    break;
                }
                   
            }
            //zbrišemo vsa zapolnjena polja in osvežimo tabelo z oglasi
            pictureOfAdvertisement_picturebox.Image = null;
            keywords_listbox2.Items.Clear();
            description_label3.Text = " ";
            deleteAdvertisement_button.Enabled = false;
            RefreshDataInListView();
        }
    }
}
