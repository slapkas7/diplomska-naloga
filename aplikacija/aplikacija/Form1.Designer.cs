﻿namespace aplikacija
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.uploadButtonMongoDB = new System.Windows.Forms.Button();
            this.console = new System.Windows.Forms.RichTextBox();
            this.MongoDB = new System.Windows.Forms.Label();
            this.MongoDB_Status = new System.Windows.Forms.Label();
            this.PredictionIO = new System.Windows.Forms.Label();
            this.PredictionIO_Status = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.mainStripMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.checkConnection = new System.Windows.Forms.ToolStripMenuItem();
            this.settings = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.preprocessingPage = new System.Windows.Forms.TabPage();
            this.predictionIOUpload_groupbox = new System.Windows.Forms.GroupBox();
            this.numberOfUsersToUpload_label = new System.Windows.Forms.Label();
            this.numberOfUsersToUpload_textbox = new System.Windows.Forms.TextBox();
            this.uploadButtonPredictionIO = new System.Windows.Forms.Button();
            this.preprocessing_groupBox = new System.Windows.Forms.GroupBox();
            this.numberOfLogs_checkbox = new System.Windows.Forms.CheckBox();
            this.logs_textbox = new System.Windows.Forms.RichTextBox();
            this.logs_saveButton = new System.Windows.Forms.Button();
            this.json_saveButton = new System.Windows.Forms.Button();
            this.logs_searchButton = new System.Windows.Forms.Button();
            this.log_saveButton = new System.Windows.Forms.Button();
            this.json_searchButton = new System.Windows.Forms.Button();
            this.logs_label = new System.Windows.Forms.Label();
            this.MediaID_saveButton = new System.Windows.Forms.Button();
            this.json_label = new System.Windows.Forms.Label();
            this.json_textbox = new System.Windows.Forms.TextBox();
            this.log_searchButton = new System.Windows.Forms.Button();
            this.MediaID_searchButton = new System.Windows.Forms.Button();
            this.log_label = new System.Windows.Forms.Label();
            this.log_textbox = new System.Windows.Forms.TextBox();
            this.mediaID_label = new System.Windows.Forms.Label();
            this.mediaID_textbox = new System.Windows.Forms.TextBox();
            this.advertisementPage = new System.Windows.Forms.TabPage();
            this.deleteAdvertisement_button = new System.Windows.Forms.Button();
            this.keywords_listbox2 = new System.Windows.Forms.ListBox();
            this.description_label3 = new System.Windows.Forms.Label();
            this.description_label2 = new System.Windows.Forms.Label();
            this.keywords_label = new System.Windows.Forms.Label();
            this.pictureOfAdvertisement_label = new System.Windows.Forms.Label();
            this.pictureOfAdvertisement_picturebox = new System.Windows.Forms.PictureBox();
            this.advertisement_listview = new System.Windows.Forms.ListView();
            this.advertisements_groupBox = new System.Windows.Forms.GroupBox();
            this.addKeyword_button = new System.Windows.Forms.Button();
            this.addNewAdvertisement_button = new System.Windows.Forms.Button();
            this.keywords_listbox = new System.Windows.Forms.ListBox();
            this.keyword_textbox = new System.Windows.Forms.TextBox();
            this.keyword_label = new System.Windows.Forms.Label();
            this.searchPicture_button = new System.Windows.Forms.Button();
            this.pathToPicture_textbox = new System.Windows.Forms.TextBox();
            this.pathToPicture_label = new System.Windows.Forms.Label();
            this.description_textbox = new System.Windows.Forms.TextBox();
            this.description_label = new System.Windows.Forms.Label();
            this.name_textbox = new System.Windows.Forms.TextBox();
            this.name_label = new System.Windows.Forms.Label();
            this.saveAdvertisements_button = new System.Windows.Forms.Button();
            this.openAdvertisements_button = new System.Windows.Forms.Button();
            this.openAdvertisements_textbox = new System.Windows.Forms.TextBox();
            this.searchAdvertisements_button = new System.Windows.Forms.Button();
            this.openAdvertisements_label = new System.Windows.Forms.Label();
            this.predictionPage = new System.Windows.Forms.TabPage();
            this.numberOfMatches_label = new System.Windows.Forms.Label();
            this.numberOfMatches_listview = new System.Windows.Forms.ListView();
            this.keywordsFromAdvertisement_label = new System.Windows.Forms.Label();
            this.keywordsFromAdvertisement_listbox = new System.Windows.Forms.ListBox();
            this.recommendedAdvertisements_listview = new System.Windows.Forms.ListView();
            this.rangeOfRecommendation_label = new System.Windows.Forms.Label();
            this.rangeOfRecommendation_textbox = new System.Windows.Forms.TextBox();
            this.predictionPercentage_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.predictionNumbers_chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.getUsers_button = new System.Windows.Forms.Button();
            this.users_listbox = new System.Windows.Forms.ListBox();
            this.PredictionIOEngine = new System.Windows.Forms.Label();
            this.PredictionIOEngine_Status = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.preprocessingPage.SuspendLayout();
            this.predictionIOUpload_groupbox.SuspendLayout();
            this.preprocessing_groupBox.SuspendLayout();
            this.advertisementPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureOfAdvertisement_picturebox)).BeginInit();
            this.advertisements_groupBox.SuspendLayout();
            this.predictionPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.predictionPercentage_chart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.predictionNumbers_chart)).BeginInit();
            this.SuspendLayout();
            // 
            // uploadButtonMongoDB
            // 
            this.uploadButtonMongoDB.Location = new System.Drawing.Point(308, 328);
            this.uploadButtonMongoDB.Name = "uploadButtonMongoDB";
            this.uploadButtonMongoDB.Size = new System.Drawing.Size(109, 23);
            this.uploadButtonMongoDB.TabIndex = 1;
            this.uploadButtonMongoDB.Text = "Naloži v MongoDB";
            this.uploadButtonMongoDB.UseVisualStyleBackColor = true;
            this.uploadButtonMongoDB.Click += new System.EventHandler(this.UploadButton_Click);
            // 
            // console
            // 
            this.console.Location = new System.Drawing.Point(12, 681);
            this.console.Name = "console";
            this.console.Size = new System.Drawing.Size(1340, 147);
            this.console.TabIndex = 2;
            this.console.Text = "";
            // 
            // MongoDB
            // 
            this.MongoDB.AutoSize = true;
            this.MongoDB.Location = new System.Drawing.Point(9, 831);
            this.MongoDB.Name = "MongoDB";
            this.MongoDB.Size = new System.Drawing.Size(58, 13);
            this.MongoDB.TabIndex = 3;
            this.MongoDB.Text = "MongoDB:";
            // 
            // MongoDB_Status
            // 
            this.MongoDB_Status.AutoSize = true;
            this.MongoDB_Status.ForeColor = System.Drawing.Color.Red;
            this.MongoDB_Status.Location = new System.Drawing.Point(73, 831);
            this.MongoDB_Status.Name = "MongoDB_Status";
            this.MongoDB_Status.Size = new System.Drawing.Size(61, 13);
            this.MongoDB_Status.TabIndex = 4;
            this.MongoDB_Status.Text = "Ni povezan";
            // 
            // PredictionIO
            // 
            this.PredictionIO.AutoSize = true;
            this.PredictionIO.Location = new System.Drawing.Point(207, 831);
            this.PredictionIO.Name = "PredictionIO";
            this.PredictionIO.Size = new System.Drawing.Size(127, 13);
            this.PredictionIO.TabIndex = 5;
            this.PredictionIO.Text = "PredictionIO event server";
            // 
            // PredictionIO_Status
            // 
            this.PredictionIO_Status.AutoSize = true;
            this.PredictionIO_Status.ForeColor = System.Drawing.Color.Red;
            this.PredictionIO_Status.Location = new System.Drawing.Point(340, 831);
            this.PredictionIO_Status.Name = "PredictionIO_Status";
            this.PredictionIO_Status.Size = new System.Drawing.Size(61, 13);
            this.PredictionIO_Status.TabIndex = 6;
            this.PredictionIO_Status.Text = "Ni povezan";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mainStripMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1356, 24);
            this.menuStrip1.TabIndex = 17;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // mainStripMenu
            // 
            this.mainStripMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkConnection,
            this.settings});
            this.mainStripMenu.Name = "mainStripMenu";
            this.mainStripMenu.Size = new System.Drawing.Size(66, 20);
            this.mainStripMenu.Text = "Datoteke";
            // 
            // checkConnection
            // 
            this.checkConnection.Name = "checkConnection";
            this.checkConnection.Size = new System.Drawing.Size(180, 22);
            this.checkConnection.Text = "Preveri povezave";
            this.checkConnection.Click += new System.EventHandler(this.CheckConnection_Click);
            // 
            // settings
            // 
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(180, 22);
            this.settings.Text = "Nastavitve";
            this.settings.Click += new System.EventHandler(this.Settings_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.preprocessingPage);
            this.tabControl1.Controls.Add(this.advertisementPage);
            this.tabControl1.Controls.Add(this.predictionPage);
            this.tabControl1.Location = new System.Drawing.Point(0, 20);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1356, 655);
            this.tabControl1.TabIndex = 18;
            // 
            // preprocessingPage
            // 
            this.preprocessingPage.Controls.Add(this.predictionIOUpload_groupbox);
            this.preprocessingPage.Controls.Add(this.preprocessing_groupBox);
            this.preprocessingPage.Location = new System.Drawing.Point(4, 22);
            this.preprocessingPage.Name = "preprocessingPage";
            this.preprocessingPage.Padding = new System.Windows.Forms.Padding(3);
            this.preprocessingPage.Size = new System.Drawing.Size(1348, 629);
            this.preprocessingPage.TabIndex = 0;
            this.preprocessingPage.Text = "Predprocesiranje";
            this.preprocessingPage.UseVisualStyleBackColor = true;
            // 
            // predictionIOUpload_groupbox
            // 
            this.predictionIOUpload_groupbox.Controls.Add(this.numberOfUsersToUpload_label);
            this.predictionIOUpload_groupbox.Controls.Add(this.numberOfUsersToUpload_textbox);
            this.predictionIOUpload_groupbox.Controls.Add(this.uploadButtonPredictionIO);
            this.predictionIOUpload_groupbox.Location = new System.Drawing.Point(465, 427);
            this.predictionIOUpload_groupbox.Name = "predictionIOUpload_groupbox";
            this.predictionIOUpload_groupbox.Size = new System.Drawing.Size(386, 163);
            this.predictionIOUpload_groupbox.TabIndex = 22;
            this.predictionIOUpload_groupbox.TabStop = false;
            this.predictionIOUpload_groupbox.Text = "Nalaganje podatkov v PredictionIO";
            // 
            // numberOfUsersToUpload_label
            // 
            this.numberOfUsersToUpload_label.AutoSize = true;
            this.numberOfUsersToUpload_label.Location = new System.Drawing.Point(6, 34);
            this.numberOfUsersToUpload_label.Name = "numberOfUsersToUpload_label";
            this.numberOfUsersToUpload_label.Size = new System.Drawing.Size(215, 13);
            this.numberOfUsersToUpload_label.TabIndex = 23;
            this.numberOfUsersToUpload_label.Text = "Število uporabnikov za PredictionIO strežnik";
            // 
            // numberOfUsersToUpload_textbox
            // 
            this.numberOfUsersToUpload_textbox.Location = new System.Drawing.Point(6, 50);
            this.numberOfUsersToUpload_textbox.Name = "numberOfUsersToUpload_textbox";
            this.numberOfUsersToUpload_textbox.Size = new System.Drawing.Size(352, 20);
            this.numberOfUsersToUpload_textbox.TabIndex = 22;
            this.numberOfUsersToUpload_textbox.Text = "1000";
            // 
            // uploadButtonPredictionIO
            // 
            this.uploadButtonPredictionIO.Location = new System.Drawing.Point(108, 110);
            this.uploadButtonPredictionIO.Name = "uploadButtonPredictionIO";
            this.uploadButtonPredictionIO.Size = new System.Drawing.Size(144, 23);
            this.uploadButtonPredictionIO.TabIndex = 21;
            this.uploadButtonPredictionIO.Text = "Naloži v PredictionIO";
            this.uploadButtonPredictionIO.UseVisualStyleBackColor = true;
            this.uploadButtonPredictionIO.Click += new System.EventHandler(this.UploadButtonPredictionIO_Click);
            // 
            // preprocessing_groupBox
            // 
            this.preprocessing_groupBox.Controls.Add(this.numberOfLogs_checkbox);
            this.preprocessing_groupBox.Controls.Add(this.uploadButtonMongoDB);
            this.preprocessing_groupBox.Controls.Add(this.logs_textbox);
            this.preprocessing_groupBox.Controls.Add(this.logs_saveButton);
            this.preprocessing_groupBox.Controls.Add(this.json_saveButton);
            this.preprocessing_groupBox.Controls.Add(this.logs_searchButton);
            this.preprocessing_groupBox.Controls.Add(this.log_saveButton);
            this.preprocessing_groupBox.Controls.Add(this.json_searchButton);
            this.preprocessing_groupBox.Controls.Add(this.logs_label);
            this.preprocessing_groupBox.Controls.Add(this.MediaID_saveButton);
            this.preprocessing_groupBox.Controls.Add(this.json_label);
            this.preprocessing_groupBox.Controls.Add(this.json_textbox);
            this.preprocessing_groupBox.Controls.Add(this.log_searchButton);
            this.preprocessing_groupBox.Controls.Add(this.MediaID_searchButton);
            this.preprocessing_groupBox.Controls.Add(this.log_label);
            this.preprocessing_groupBox.Controls.Add(this.log_textbox);
            this.preprocessing_groupBox.Controls.Add(this.mediaID_label);
            this.preprocessing_groupBox.Controls.Add(this.mediaID_textbox);
            this.preprocessing_groupBox.Location = new System.Drawing.Point(240, 20);
            this.preprocessing_groupBox.Name = "preprocessing_groupBox";
            this.preprocessing_groupBox.Size = new System.Drawing.Size(856, 366);
            this.preprocessing_groupBox.TabIndex = 15;
            this.preprocessing_groupBox.TabStop = false;
            this.preprocessing_groupBox.Text = "Predprocesiranje";
            // 
            // numberOfLogs_checkbox
            // 
            this.numberOfLogs_checkbox.AutoSize = true;
            this.numberOfLogs_checkbox.Location = new System.Drawing.Point(276, 19);
            this.numberOfLogs_checkbox.Name = "numberOfLogs_checkbox";
            this.numberOfLogs_checkbox.Size = new System.Drawing.Size(90, 17);
            this.numberOfLogs_checkbox.TabIndex = 20;
            this.numberOfLogs_checkbox.Text = "Ena datoteka";
            this.numberOfLogs_checkbox.UseVisualStyleBackColor = true;
            this.numberOfLogs_checkbox.CheckedChanged += new System.EventHandler(this.NumberOFLogs_CheckedChanged);
            // 
            // logs_textbox
            // 
            this.logs_textbox.Location = new System.Drawing.Point(402, 44);
            this.logs_textbox.Name = "logs_textbox";
            this.logs_textbox.Size = new System.Drawing.Size(420, 216);
            this.logs_textbox.TabIndex = 19;
            this.logs_textbox.Text = "";
            // 
            // logs_saveButton
            // 
            this.logs_saveButton.Location = new System.Drawing.Point(483, 267);
            this.logs_saveButton.Name = "logs_saveButton";
            this.logs_saveButton.Size = new System.Drawing.Size(75, 23);
            this.logs_saveButton.TabIndex = 3;
            this.logs_saveButton.Text = "Shrani";
            this.logs_saveButton.UseVisualStyleBackColor = true;
            this.logs_saveButton.Click += new System.EventHandler(this.Logs_saveButton_Click);
            // 
            // json_saveButton
            // 
            this.json_saveButton.Location = new System.Drawing.Point(87, 267);
            this.json_saveButton.Name = "json_saveButton";
            this.json_saveButton.Size = new System.Drawing.Size(75, 23);
            this.json_saveButton.TabIndex = 3;
            this.json_saveButton.Text = "Shrani";
            this.json_saveButton.UseVisualStyleBackColor = true;
            this.json_saveButton.Click += new System.EventHandler(this.Json_saveButton_Click);
            // 
            // logs_searchButton
            // 
            this.logs_searchButton.Location = new System.Drawing.Point(402, 267);
            this.logs_searchButton.Name = "logs_searchButton";
            this.logs_searchButton.Size = new System.Drawing.Size(75, 23);
            this.logs_searchButton.TabIndex = 2;
            this.logs_searchButton.Text = "Poišči";
            this.logs_searchButton.UseVisualStyleBackColor = true;
            this.logs_searchButton.Click += new System.EventHandler(this.Logs_searchButton_Click);
            // 
            // log_saveButton
            // 
            this.log_saveButton.Location = new System.Drawing.Point(87, 163);
            this.log_saveButton.Name = "log_saveButton";
            this.log_saveButton.Size = new System.Drawing.Size(75, 23);
            this.log_saveButton.TabIndex = 3;
            this.log_saveButton.Text = "Shrani";
            this.log_saveButton.UseVisualStyleBackColor = true;
            this.log_saveButton.Click += new System.EventHandler(this.Log_saveButton_Click);
            // 
            // json_searchButton
            // 
            this.json_searchButton.Location = new System.Drawing.Point(6, 267);
            this.json_searchButton.Name = "json_searchButton";
            this.json_searchButton.Size = new System.Drawing.Size(75, 23);
            this.json_searchButton.TabIndex = 2;
            this.json_searchButton.Text = "Poišči";
            this.json_searchButton.UseVisualStyleBackColor = true;
            this.json_searchButton.Click += new System.EventHandler(this.Json_searchButton_Click);
            // 
            // logs_label
            // 
            this.logs_label.AutoSize = true;
            this.logs_label.Location = new System.Drawing.Point(399, 28);
            this.logs_label.Name = "logs_label";
            this.logs_label.Size = new System.Drawing.Size(103, 13);
            this.logs_label.TabIndex = 1;
            this.logs_label.Text = "Dnevniške datoteke";
            // 
            // MediaID_saveButton
            // 
            this.MediaID_saveButton.Location = new System.Drawing.Point(87, 71);
            this.MediaID_saveButton.Name = "MediaID_saveButton";
            this.MediaID_saveButton.Size = new System.Drawing.Size(75, 23);
            this.MediaID_saveButton.TabIndex = 3;
            this.MediaID_saveButton.Text = "Shrani";
            this.MediaID_saveButton.UseVisualStyleBackColor = true;
            this.MediaID_saveButton.Click += new System.EventHandler(this.MediaID_saveButton_Click);
            // 
            // json_label
            // 
            this.json_label.AutoSize = true;
            this.json_label.Location = new System.Drawing.Point(6, 224);
            this.json_label.Name = "json_label";
            this.json_label.Size = new System.Drawing.Size(127, 13);
            this.json_label.TabIndex = 1;
            this.json_label.Text = "Izhodna datoteka (JSON)";
            // 
            // json_textbox
            // 
            this.json_textbox.Location = new System.Drawing.Point(6, 240);
            this.json_textbox.Name = "json_textbox";
            this.json_textbox.Size = new System.Drawing.Size(352, 20);
            this.json_textbox.TabIndex = 0;
            // 
            // log_searchButton
            // 
            this.log_searchButton.Location = new System.Drawing.Point(6, 163);
            this.log_searchButton.Name = "log_searchButton";
            this.log_searchButton.Size = new System.Drawing.Size(75, 23);
            this.log_searchButton.TabIndex = 2;
            this.log_searchButton.Text = "Poišči";
            this.log_searchButton.UseVisualStyleBackColor = true;
            this.log_searchButton.Click += new System.EventHandler(this.Log_searchButton_Click);
            // 
            // MediaID_searchButton
            // 
            this.MediaID_searchButton.Location = new System.Drawing.Point(6, 71);
            this.MediaID_searchButton.Name = "MediaID_searchButton";
            this.MediaID_searchButton.Size = new System.Drawing.Size(75, 23);
            this.MediaID_searchButton.TabIndex = 2;
            this.MediaID_searchButton.Text = "Poišči";
            this.MediaID_searchButton.UseVisualStyleBackColor = true;
            this.MediaID_searchButton.Click += new System.EventHandler(this.MediaID_searchButton_Click);
            // 
            // log_label
            // 
            this.log_label.AutoSize = true;
            this.log_label.Location = new System.Drawing.Point(6, 120);
            this.log_label.Name = "log_label";
            this.log_label.Size = new System.Drawing.Size(103, 13);
            this.log_label.TabIndex = 1;
            this.log_label.Text = "Dnevniška datoteka";
            // 
            // log_textbox
            // 
            this.log_textbox.Location = new System.Drawing.Point(6, 136);
            this.log_textbox.Name = "log_textbox";
            this.log_textbox.Size = new System.Drawing.Size(352, 20);
            this.log_textbox.TabIndex = 0;
            // 
            // mediaID_label
            // 
            this.mediaID_label.AutoSize = true;
            this.mediaID_label.Location = new System.Drawing.Point(6, 28);
            this.mediaID_label.Name = "mediaID_label";
            this.mediaID_label.Size = new System.Drawing.Size(136, 13);
            this.mediaID_label.TabIndex = 1;
            this.mediaID_label.Text = "Datoteka z opisom kanalov";
            // 
            // mediaID_textbox
            // 
            this.mediaID_textbox.Location = new System.Drawing.Point(6, 44);
            this.mediaID_textbox.Name = "mediaID_textbox";
            this.mediaID_textbox.Size = new System.Drawing.Size(352, 20);
            this.mediaID_textbox.TabIndex = 0;
            // 
            // advertisementPage
            // 
            this.advertisementPage.Controls.Add(this.deleteAdvertisement_button);
            this.advertisementPage.Controls.Add(this.keywords_listbox2);
            this.advertisementPage.Controls.Add(this.description_label3);
            this.advertisementPage.Controls.Add(this.description_label2);
            this.advertisementPage.Controls.Add(this.keywords_label);
            this.advertisementPage.Controls.Add(this.pictureOfAdvertisement_label);
            this.advertisementPage.Controls.Add(this.pictureOfAdvertisement_picturebox);
            this.advertisementPage.Controls.Add(this.advertisement_listview);
            this.advertisementPage.Controls.Add(this.advertisements_groupBox);
            this.advertisementPage.Controls.Add(this.saveAdvertisements_button);
            this.advertisementPage.Controls.Add(this.openAdvertisements_button);
            this.advertisementPage.Controls.Add(this.openAdvertisements_textbox);
            this.advertisementPage.Controls.Add(this.searchAdvertisements_button);
            this.advertisementPage.Controls.Add(this.openAdvertisements_label);
            this.advertisementPage.Location = new System.Drawing.Point(4, 22);
            this.advertisementPage.Name = "advertisementPage";
            this.advertisementPage.Padding = new System.Windows.Forms.Padding(3);
            this.advertisementPage.Size = new System.Drawing.Size(1348, 629);
            this.advertisementPage.TabIndex = 1;
            this.advertisementPage.Text = "Oglasi";
            this.advertisementPage.UseVisualStyleBackColor = true;
            // 
            // deleteAdvertisement_button
            // 
            this.deleteAdvertisement_button.Location = new System.Drawing.Point(1049, 569);
            this.deleteAdvertisement_button.Name = "deleteAdvertisement_button";
            this.deleteAdvertisement_button.Size = new System.Drawing.Size(115, 23);
            this.deleteAdvertisement_button.TabIndex = 29;
            this.deleteAdvertisement_button.Text = "Izbriši oglas";
            this.deleteAdvertisement_button.UseVisualStyleBackColor = true;
            this.deleteAdvertisement_button.Click += new System.EventHandler(this.DeleteAdvertisement_button_Click);
            // 
            // keywords_listbox2
            // 
            this.keywords_listbox2.FormattingEnabled = true;
            this.keywords_listbox2.Location = new System.Drawing.Point(989, 352);
            this.keywords_listbox2.Name = "keywords_listbox2";
            this.keywords_listbox2.Size = new System.Drawing.Size(212, 95);
            this.keywords_listbox2.TabIndex = 28;
            // 
            // description_label3
            // 
            this.description_label3.AutoSize = true;
            this.description_label3.Location = new System.Drawing.Point(986, 507);
            this.description_label3.Name = "description_label3";
            this.description_label3.Size = new System.Drawing.Size(33, 13);
            this.description_label3.TabIndex = 27;
            this.description_label3.Text = "#opis";
            // 
            // description_label2
            // 
            this.description_label2.AutoSize = true;
            this.description_label2.Location = new System.Drawing.Point(986, 484);
            this.description_label2.Name = "description_label2";
            this.description_label2.Size = new System.Drawing.Size(63, 13);
            this.description_label2.TabIndex = 26;
            this.description_label2.Text = "Kratek opis:";
            // 
            // keywords_label
            // 
            this.keywords_label.AutoSize = true;
            this.keywords_label.Location = new System.Drawing.Point(986, 332);
            this.keywords_label.Name = "keywords_label";
            this.keywords_label.Size = new System.Drawing.Size(83, 13);
            this.keywords_label.TabIndex = 25;
            this.keywords_label.Text = "Ključne besede:";
            // 
            // pictureOfAdvertisement_label
            // 
            this.pictureOfAdvertisement_label.AutoSize = true;
            this.pictureOfAdvertisement_label.Location = new System.Drawing.Point(577, 328);
            this.pictureOfAdvertisement_label.Name = "pictureOfAdvertisement_label";
            this.pictureOfAdvertisement_label.Size = new System.Drawing.Size(64, 13);
            this.pictureOfAdvertisement_label.TabIndex = 23;
            this.pictureOfAdvertisement_label.Text = "Slika oglasa";
            // 
            // pictureOfAdvertisement_picturebox
            // 
            this.pictureOfAdvertisement_picturebox.Location = new System.Drawing.Point(339, 353);
            this.pictureOfAdvertisement_picturebox.Name = "pictureOfAdvertisement_picturebox";
            this.pictureOfAdvertisement_picturebox.Size = new System.Drawing.Size(555, 273);
            this.pictureOfAdvertisement_picturebox.TabIndex = 22;
            this.pictureOfAdvertisement_picturebox.TabStop = false;
            // 
            // advertisement_listview
            // 
            this.advertisement_listview.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.advertisement_listview.Location = new System.Drawing.Point(339, 35);
            this.advertisement_listview.Name = "advertisement_listview";
            this.advertisement_listview.Size = new System.Drawing.Size(980, 277);
            this.advertisement_listview.TabIndex = 21;
            this.advertisement_listview.UseCompatibleStateImageBehavior = false;
            this.advertisement_listview.DoubleClick += new System.EventHandler(this.Advertisements_DoubleClick);
            // 
            // advertisements_groupBox
            // 
            this.advertisements_groupBox.Controls.Add(this.addKeyword_button);
            this.advertisements_groupBox.Controls.Add(this.addNewAdvertisement_button);
            this.advertisements_groupBox.Controls.Add(this.keywords_listbox);
            this.advertisements_groupBox.Controls.Add(this.keyword_textbox);
            this.advertisements_groupBox.Controls.Add(this.keyword_label);
            this.advertisements_groupBox.Controls.Add(this.searchPicture_button);
            this.advertisements_groupBox.Controls.Add(this.pathToPicture_textbox);
            this.advertisements_groupBox.Controls.Add(this.pathToPicture_label);
            this.advertisements_groupBox.Controls.Add(this.description_textbox);
            this.advertisements_groupBox.Controls.Add(this.description_label);
            this.advertisements_groupBox.Controls.Add(this.name_textbox);
            this.advertisements_groupBox.Controls.Add(this.name_label);
            this.advertisements_groupBox.Location = new System.Drawing.Point(39, 161);
            this.advertisements_groupBox.Name = "advertisements_groupBox";
            this.advertisements_groupBox.Size = new System.Drawing.Size(236, 460);
            this.advertisements_groupBox.TabIndex = 20;
            this.advertisements_groupBox.TabStop = false;
            this.advertisements_groupBox.Text = "Dodajanje oglasa";
            // 
            // addKeyword_button
            // 
            this.addKeyword_button.Location = new System.Drawing.Point(6, 244);
            this.addKeyword_button.Name = "addKeyword_button";
            this.addKeyword_button.Size = new System.Drawing.Size(220, 23);
            this.addKeyword_button.TabIndex = 22;
            this.addKeyword_button.Text = "Dodaj ključno besedo";
            this.addKeyword_button.UseVisualStyleBackColor = true;
            this.addKeyword_button.Click += new System.EventHandler(this.AddKeyword_button_Click);
            // 
            // addNewAdvertisement_button
            // 
            this.addNewAdvertisement_button.Location = new System.Drawing.Point(69, 408);
            this.addNewAdvertisement_button.Name = "addNewAdvertisement_button";
            this.addNewAdvertisement_button.Size = new System.Drawing.Size(75, 23);
            this.addNewAdvertisement_button.TabIndex = 21;
            this.addNewAdvertisement_button.Text = "Dodaj oglas";
            this.addNewAdvertisement_button.UseVisualStyleBackColor = true;
            this.addNewAdvertisement_button.Click += new System.EventHandler(this.AddNewAdvertisement_button_Click);
            // 
            // keywords_listbox
            // 
            this.keywords_listbox.FormattingEnabled = true;
            this.keywords_listbox.Location = new System.Drawing.Point(7, 282);
            this.keywords_listbox.Name = "keywords_listbox";
            this.keywords_listbox.Size = new System.Drawing.Size(219, 108);
            this.keywords_listbox.TabIndex = 20;
            // 
            // keyword_textbox
            // 
            this.keyword_textbox.Location = new System.Drawing.Point(6, 218);
            this.keyword_textbox.Name = "keyword_textbox";
            this.keyword_textbox.Size = new System.Drawing.Size(220, 20);
            this.keyword_textbox.TabIndex = 19;
            // 
            // keyword_label
            // 
            this.keyword_label.AutoSize = true;
            this.keyword_label.Location = new System.Drawing.Point(4, 202);
            this.keyword_label.Name = "keyword_label";
            this.keyword_label.Size = new System.Drawing.Size(83, 13);
            this.keyword_label.TabIndex = 18;
            this.keyword_label.Text = "Ključna besede:";
            // 
            // searchPicture_button
            // 
            this.searchPicture_button.Location = new System.Drawing.Point(10, 157);
            this.searchPicture_button.Name = "searchPicture_button";
            this.searchPicture_button.Size = new System.Drawing.Size(115, 23);
            this.searchPicture_button.TabIndex = 15;
            this.searchPicture_button.Text = "Poišči datoteko";
            this.searchPicture_button.UseVisualStyleBackColor = true;
            this.searchPicture_button.Click += new System.EventHandler(this.SearchPicture_button_Click);
            // 
            // pathToPicture_textbox
            // 
            this.pathToPicture_textbox.Location = new System.Drawing.Point(10, 131);
            this.pathToPicture_textbox.Name = "pathToPicture_textbox";
            this.pathToPicture_textbox.Size = new System.Drawing.Size(220, 20);
            this.pathToPicture_textbox.TabIndex = 15;
            // 
            // pathToPicture_label
            // 
            this.pathToPicture_label.AutoSize = true;
            this.pathToPicture_label.Location = new System.Drawing.Point(7, 114);
            this.pathToPicture_label.Name = "pathToPicture_label";
            this.pathToPicture_label.Size = new System.Drawing.Size(65, 13);
            this.pathToPicture_label.TabIndex = 15;
            this.pathToPicture_label.Text = "Pot do slike:";
            // 
            // description_textbox
            // 
            this.description_textbox.Location = new System.Drawing.Point(10, 81);
            this.description_textbox.Name = "description_textbox";
            this.description_textbox.Size = new System.Drawing.Size(220, 20);
            this.description_textbox.TabIndex = 17;
            // 
            // description_label
            // 
            this.description_label.AutoSize = true;
            this.description_label.Location = new System.Drawing.Point(7, 62);
            this.description_label.Name = "description_label";
            this.description_label.Size = new System.Drawing.Size(63, 13);
            this.description_label.TabIndex = 16;
            this.description_label.Text = "Kratek opis:";
            // 
            // name_textbox
            // 
            this.name_textbox.Location = new System.Drawing.Point(10, 36);
            this.name_textbox.Name = "name_textbox";
            this.name_textbox.Size = new System.Drawing.Size(220, 20);
            this.name_textbox.TabIndex = 15;
            // 
            // name_label
            // 
            this.name_label.AutoSize = true;
            this.name_label.Location = new System.Drawing.Point(7, 20);
            this.name_label.Name = "name_label";
            this.name_label.Size = new System.Drawing.Size(61, 13);
            this.name_label.TabIndex = 0;
            this.name_label.Text = "Ime oglasa:";
            // 
            // saveAdvertisements_button
            // 
            this.saveAdvertisements_button.Location = new System.Drawing.Point(39, 102);
            this.saveAdvertisements_button.Name = "saveAdvertisements_button";
            this.saveAdvertisements_button.Size = new System.Drawing.Size(106, 23);
            this.saveAdvertisements_button.TabIndex = 19;
            this.saveAdvertisements_button.Text = "Shrani oglase";
            this.saveAdvertisements_button.UseVisualStyleBackColor = true;
            this.saveAdvertisements_button.Click += new System.EventHandler(this.SaveAdvertisements_button_Click);
            // 
            // openAdvertisements_button
            // 
            this.openAdvertisements_button.Location = new System.Drawing.Point(39, 65);
            this.openAdvertisements_button.Name = "openAdvertisements_button";
            this.openAdvertisements_button.Size = new System.Drawing.Size(106, 23);
            this.openAdvertisements_button.TabIndex = 18;
            this.openAdvertisements_button.Text = "Odpri oglase";
            this.openAdvertisements_button.UseVisualStyleBackColor = true;
            this.openAdvertisements_button.Click += new System.EventHandler(this.OpenAdvertisements_button_Click);
            // 
            // openAdvertisements_textbox
            // 
            this.openAdvertisements_textbox.Location = new System.Drawing.Point(39, 39);
            this.openAdvertisements_textbox.Name = "openAdvertisements_textbox";
            this.openAdvertisements_textbox.Size = new System.Drawing.Size(246, 20);
            this.openAdvertisements_textbox.TabIndex = 17;
            // 
            // searchAdvertisements_button
            // 
            this.searchAdvertisements_button.Location = new System.Drawing.Point(150, 65);
            this.searchAdvertisements_button.Name = "searchAdvertisements_button";
            this.searchAdvertisements_button.Size = new System.Drawing.Size(115, 23);
            this.searchAdvertisements_button.TabIndex = 16;
            this.searchAdvertisements_button.Text = "Poišči datoteko";
            this.searchAdvertisements_button.UseVisualStyleBackColor = true;
            this.searchAdvertisements_button.Click += new System.EventHandler(this.SearchAdvertisements_button_Click);
            // 
            // openAdvertisements_label
            // 
            this.openAdvertisements_label.AutoSize = true;
            this.openAdvertisements_label.Location = new System.Drawing.Point(36, 19);
            this.openAdvertisements_label.Name = "openAdvertisements_label";
            this.openAdvertisements_label.Size = new System.Drawing.Size(115, 13);
            this.openAdvertisements_label.TabIndex = 15;
            this.openAdvertisements_label.Text = "Odpri shranjene oglase";
            // 
            // predictionPage
            // 
            this.predictionPage.Controls.Add(this.numberOfMatches_label);
            this.predictionPage.Controls.Add(this.numberOfMatches_listview);
            this.predictionPage.Controls.Add(this.keywordsFromAdvertisement_label);
            this.predictionPage.Controls.Add(this.keywordsFromAdvertisement_listbox);
            this.predictionPage.Controls.Add(this.recommendedAdvertisements_listview);
            this.predictionPage.Controls.Add(this.rangeOfRecommendation_label);
            this.predictionPage.Controls.Add(this.rangeOfRecommendation_textbox);
            this.predictionPage.Controls.Add(this.predictionPercentage_chart);
            this.predictionPage.Controls.Add(this.predictionNumbers_chart);
            this.predictionPage.Controls.Add(this.getUsers_button);
            this.predictionPage.Controls.Add(this.users_listbox);
            this.predictionPage.Location = new System.Drawing.Point(4, 22);
            this.predictionPage.Name = "predictionPage";
            this.predictionPage.Padding = new System.Windows.Forms.Padding(3);
            this.predictionPage.Size = new System.Drawing.Size(1348, 629);
            this.predictionPage.TabIndex = 2;
            this.predictionPage.Text = "Priporočila";
            this.predictionPage.UseVisualStyleBackColor = true;
            // 
            // numberOfMatches_label
            // 
            this.numberOfMatches_label.AutoSize = true;
            this.numberOfMatches_label.Location = new System.Drawing.Point(427, 388);
            this.numberOfMatches_label.Name = "numberOfMatches_label";
            this.numberOfMatches_label.Size = new System.Drawing.Size(89, 13);
            this.numberOfMatches_label.TabIndex = 29;
            this.numberOfMatches_label.Text = "Število zadetkov:";
            // 
            // numberOfMatches_listview
            // 
            this.numberOfMatches_listview.Location = new System.Drawing.Point(430, 417);
            this.numberOfMatches_listview.Name = "numberOfMatches_listview";
            this.numberOfMatches_listview.Size = new System.Drawing.Size(121, 173);
            this.numberOfMatches_listview.TabIndex = 28;
            this.numberOfMatches_listview.UseCompatibleStateImageBehavior = false;
            // 
            // keywordsFromAdvertisement_label
            // 
            this.keywordsFromAdvertisement_label.AutoSize = true;
            this.keywordsFromAdvertisement_label.Location = new System.Drawing.Point(272, 388);
            this.keywordsFromAdvertisement_label.Name = "keywordsFromAdvertisement_label";
            this.keywordsFromAdvertisement_label.Size = new System.Drawing.Size(83, 13);
            this.keywordsFromAdvertisement_label.TabIndex = 27;
            this.keywordsFromAdvertisement_label.Text = "Ključne besede:";
            // 
            // keywordsFromAdvertisement_listbox
            // 
            this.keywordsFromAdvertisement_listbox.FormattingEnabled = true;
            this.keywordsFromAdvertisement_listbox.Location = new System.Drawing.Point(275, 417);
            this.keywordsFromAdvertisement_listbox.Name = "keywordsFromAdvertisement_listbox";
            this.keywordsFromAdvertisement_listbox.Size = new System.Drawing.Size(120, 173);
            this.keywordsFromAdvertisement_listbox.TabIndex = 26;
            // 
            // recommendedAdvertisements_listview
            // 
            this.recommendedAdvertisements_listview.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.recommendedAdvertisements_listview.Location = new System.Drawing.Point(272, 24);
            this.recommendedAdvertisements_listview.Name = "recommendedAdvertisements_listview";
            this.recommendedAdvertisements_listview.Size = new System.Drawing.Size(509, 344);
            this.recommendedAdvertisements_listview.TabIndex = 24;
            this.recommendedAdvertisements_listview.UseCompatibleStateImageBehavior = false;
            this.recommendedAdvertisements_listview.DoubleClick += new System.EventHandler(this.RecommendedAdvertisements_DoubleClick);
            // 
            // rangeOfRecommendation_label
            // 
            this.rangeOfRecommendation_label.AutoSize = true;
            this.rangeOfRecommendation_label.Location = new System.Drawing.Point(26, 500);
            this.rangeOfRecommendation_label.Name = "rangeOfRecommendation_label";
            this.rangeOfRecommendation_label.Size = new System.Drawing.Size(148, 13);
            this.rangeOfRecommendation_label.TabIndex = 23;
            this.rangeOfRecommendation_label.Text = "Število upoštevanih priporočil:";
            // 
            // rangeOfRecommendation_textbox
            // 
            this.rangeOfRecommendation_textbox.Location = new System.Drawing.Point(177, 497);
            this.rangeOfRecommendation_textbox.Name = "rangeOfRecommendation_textbox";
            this.rangeOfRecommendation_textbox.Size = new System.Drawing.Size(75, 20);
            this.rangeOfRecommendation_textbox.TabIndex = 22;
            this.rangeOfRecommendation_textbox.Text = "4";
            // 
            // predictionPercentage_chart
            // 
            chartArea1.Name = "ChartArea1";
            this.predictionPercentage_chart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.predictionPercentage_chart.Legends.Add(legend1);
            this.predictionPercentage_chart.Location = new System.Drawing.Point(784, 324);
            this.predictionPercentage_chart.Name = "predictionPercentage_chart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Prediction";
            this.predictionPercentage_chart.Series.Add(series1);
            this.predictionPercentage_chart.Size = new System.Drawing.Size(558, 286);
            this.predictionPercentage_chart.TabIndex = 21;
            this.predictionPercentage_chart.Text = "chart2";
            // 
            // predictionNumbers_chart
            // 
            chartArea2.Name = "ChartArea1";
            this.predictionNumbers_chart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.predictionNumbers_chart.Legends.Add(legend2);
            this.predictionNumbers_chart.Location = new System.Drawing.Point(787, 24);
            this.predictionNumbers_chart.Name = "predictionNumbers_chart";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Prediction";
            this.predictionNumbers_chart.Series.Add(series2);
            this.predictionNumbers_chart.Size = new System.Drawing.Size(558, 294);
            this.predictionNumbers_chart.TabIndex = 20;
            this.predictionNumbers_chart.Text = "chart1";
            // 
            // getUsers_button
            // 
            this.getUsers_button.Location = new System.Drawing.Point(29, 538);
            this.getUsers_button.Name = "getUsers_button";
            this.getUsers_button.Size = new System.Drawing.Size(232, 52);
            this.getUsers_button.TabIndex = 18;
            this.getUsers_button.Text = "Pridobi uporabnike";
            this.getUsers_button.UseVisualStyleBackColor = true;
            this.getUsers_button.Click += new System.EventHandler(this.GetUsers_Click);
            // 
            // users_listbox
            // 
            this.users_listbox.FormattingEnabled = true;
            this.users_listbox.Location = new System.Drawing.Point(29, 24);
            this.users_listbox.MultiColumn = true;
            this.users_listbox.Name = "users_listbox";
            this.users_listbox.Size = new System.Drawing.Size(223, 459);
            this.users_listbox.TabIndex = 19;
            this.users_listbox.DoubleClick += new System.EventHandler(this.Users_DoubleClick);
            // 
            // PredictionIOEngine
            // 
            this.PredictionIOEngine.AutoSize = true;
            this.PredictionIOEngine.Location = new System.Drawing.Point(431, 831);
            this.PredictionIOEngine.Name = "PredictionIOEngine";
            this.PredictionIOEngine.Size = new System.Drawing.Size(132, 13);
            this.PredictionIOEngine.TabIndex = 19;
            this.PredictionIOEngine.Text = "PredictionIO engine server";
            // 
            // PredictionIOEngine_Status
            // 
            this.PredictionIOEngine_Status.AutoSize = true;
            this.PredictionIOEngine_Status.ForeColor = System.Drawing.Color.Red;
            this.PredictionIOEngine_Status.Location = new System.Drawing.Point(569, 831);
            this.PredictionIOEngine_Status.Name = "PredictionIOEngine_Status";
            this.PredictionIOEngine_Status.Size = new System.Drawing.Size(61, 13);
            this.PredictionIOEngine_Status.TabIndex = 20;
            this.PredictionIOEngine_Status.Text = "Ni povezan";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1356, 849);
            this.Controls.Add(this.PredictionIOEngine_Status);
            this.Controls.Add(this.PredictionIOEngine);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.PredictionIO_Status);
            this.Controls.Add(this.PredictionIO);
            this.Controls.Add(this.MongoDB_Status);
            this.Controls.Add(this.MongoDB);
            this.Controls.Add(this.console);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Priporočilni sistem";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.preprocessingPage.ResumeLayout(false);
            this.predictionIOUpload_groupbox.ResumeLayout(false);
            this.predictionIOUpload_groupbox.PerformLayout();
            this.preprocessing_groupBox.ResumeLayout(false);
            this.preprocessing_groupBox.PerformLayout();
            this.advertisementPage.ResumeLayout(false);
            this.advertisementPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureOfAdvertisement_picturebox)).EndInit();
            this.advertisements_groupBox.ResumeLayout(false);
            this.advertisements_groupBox.PerformLayout();
            this.predictionPage.ResumeLayout(false);
            this.predictionPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.predictionPercentage_chart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.predictionNumbers_chart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button uploadButtonMongoDB;
        private System.Windows.Forms.RichTextBox console;
        private System.Windows.Forms.Label MongoDB;
        private System.Windows.Forms.Label MongoDB_Status;
        private System.Windows.Forms.Label PredictionIO;
        private System.Windows.Forms.Label PredictionIO_Status;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mainStripMenu;
        private System.Windows.Forms.ToolStripMenuItem settings;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage preprocessingPage;
        private System.Windows.Forms.TabPage advertisementPage;
        private System.Windows.Forms.TabPage predictionPage;
        private System.Windows.Forms.Label PredictionIOEngine;
        private System.Windows.Forms.Label PredictionIOEngine_Status;
        private System.Windows.Forms.Label description_label3;
        private System.Windows.Forms.Label description_label2;
        private System.Windows.Forms.Label keywords_label;
        private System.Windows.Forms.Label pictureOfAdvertisement_label;
        private System.Windows.Forms.PictureBox pictureOfAdvertisement_picturebox;
        private System.Windows.Forms.ListView advertisement_listview;
        private System.Windows.Forms.GroupBox advertisements_groupBox;
        private System.Windows.Forms.Button addKeyword_button;
        private System.Windows.Forms.Button addNewAdvertisement_button;
        private System.Windows.Forms.ListBox keywords_listbox;
        private System.Windows.Forms.TextBox keyword_textbox;
        private System.Windows.Forms.Label keyword_label;
        private System.Windows.Forms.Button searchPicture_button;
        private System.Windows.Forms.TextBox pathToPicture_textbox;
        private System.Windows.Forms.Label pathToPicture_label;
        private System.Windows.Forms.TextBox description_textbox;
        private System.Windows.Forms.Label description_label;
        private System.Windows.Forms.TextBox name_textbox;
        private System.Windows.Forms.Label name_label;
        private System.Windows.Forms.Button saveAdvertisements_button;
        private System.Windows.Forms.Button openAdvertisements_button;
        private System.Windows.Forms.TextBox openAdvertisements_textbox;
        private System.Windows.Forms.Button searchAdvertisements_button;
        private System.Windows.Forms.Label openAdvertisements_label;
        private System.Windows.Forms.Button getUsers_button;
        private System.Windows.Forms.ListBox users_listbox;
        private System.Windows.Forms.DataVisualization.Charting.Chart predictionPercentage_chart;
        private System.Windows.Forms.DataVisualization.Charting.Chart predictionNumbers_chart;
        private System.Windows.Forms.Label rangeOfRecommendation_label;
        private System.Windows.Forms.TextBox rangeOfRecommendation_textbox;
        private System.Windows.Forms.ListView recommendedAdvertisements_listview;
        private System.Windows.Forms.ToolStripMenuItem checkConnection;
        private System.Windows.Forms.ListBox keywordsFromAdvertisement_listbox;
        private System.Windows.Forms.ListBox keywords_listbox2;
        private System.Windows.Forms.ListView numberOfMatches_listview;
        private System.Windows.Forms.Label keywordsFromAdvertisement_label;
        private System.Windows.Forms.Label numberOfMatches_label;
        private System.Windows.Forms.GroupBox preprocessing_groupBox;
        private System.Windows.Forms.CheckBox numberOfLogs_checkbox;
        private System.Windows.Forms.RichTextBox logs_textbox;
        private System.Windows.Forms.Button logs_saveButton;
        private System.Windows.Forms.Button json_saveButton;
        private System.Windows.Forms.Button logs_searchButton;
        private System.Windows.Forms.Button log_saveButton;
        private System.Windows.Forms.Button json_searchButton;
        private System.Windows.Forms.Label logs_label;
        private System.Windows.Forms.Button MediaID_saveButton;
        private System.Windows.Forms.Label json_label;
        private System.Windows.Forms.TextBox json_textbox;
        private System.Windows.Forms.Button log_searchButton;
        private System.Windows.Forms.Button MediaID_searchButton;
        private System.Windows.Forms.Label log_label;
        private System.Windows.Forms.TextBox log_textbox;
        private System.Windows.Forms.Label mediaID_label;
        private System.Windows.Forms.TextBox mediaID_textbox;
        private System.Windows.Forms.GroupBox predictionIOUpload_groupbox;
        private System.Windows.Forms.Button uploadButtonPredictionIO;
        private System.Windows.Forms.Label numberOfUsersToUpload_label;
        private System.Windows.Forms.TextBox numberOfUsersToUpload_textbox;
        private System.Windows.Forms.Button deleteAdvertisement_button;
    }
}

