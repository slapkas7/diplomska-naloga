﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace aplikacija
{
    [Serializable()]
    public class Advertisement
    {
        [System.Xml.Serialization.XmlElement("Name")]
        public string Name;
        [System.Xml.Serialization.XmlElement("Description")]
        public string Description;
       // [System.Xml.Serialization.XmlElement("Keywords")]
        [XmlArray("Keywords")]
        public List<string> Keywords;
        [System.Xml.Serialization.XmlElement("Path_to_picture")]
        public string Path_to_picture;

        public Advertisement()
        {
            Name = "";
            Description = "";
            Keywords = new List<string>();
            Path_to_picture = "";
        }
        public Advertisement(string _Name, string _Description, List<string> _Keywords, string _Path_to_picture)
        {
            Name = _Name;
            Description = _Description;
            Keywords = _Keywords;
            Path_to_picture = _Path_to_picture;
        }

    }
}
