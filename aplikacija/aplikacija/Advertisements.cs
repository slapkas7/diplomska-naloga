﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace aplikacija
{
    [Serializable()]
   // [System.Xml.Serialization.XmlRoot("Oglasi")]
   [XmlRoot("Collection")]
    public class Advertisements
    {
        [XmlArray("Advertisements")]
       // [XmlArrayItem("Oglas", typeof(Oglas))]
        public List<Advertisement> Advertisement { get; set; }

        public Advertisements()
        {
            Advertisement = new List<Advertisement>();
        }
        public void SetOglas(Advertisement _advertisement)
        {
            Advertisement.Add(_advertisement);
        }
        public void RemoveAdvertisement(string _name)
        {
            for (int i = 0; i < Advertisement.Count; i++)
            {
                if(Advertisement[i].Name == _name)
                {
                    Advertisement.RemoveAt(i);
                }
            }
        }
    }
}
