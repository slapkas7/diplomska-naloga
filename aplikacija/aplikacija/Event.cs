﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aplikacija
{
    public class Event
    {
        public string @event { get; set; }
        public string entity_type { get; set; }
        public string entity_id { get; set; }
        public string target_entity_type { get; set; }
        public string target_entity_id { get; set; }
        public Rating properties { get; set; }
    }
}
